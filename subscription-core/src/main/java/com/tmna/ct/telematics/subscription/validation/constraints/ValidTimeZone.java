package com.tmna.ct.telematics.subscription.validation.constraints;

import com.tmna.ct.telematics.subscription.validation.TimeZoneValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 */
@Target({ ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = TimeZoneValidator.class)
public @interface ValidTimeZone {

    String message() default "{CODE.INVALID.INCORRECT.SUBSCRIPTION.ATTRIBUTE}:{MESSAGE.INVALID.SUBSCRIPTION.RS.TIMEZONE}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
