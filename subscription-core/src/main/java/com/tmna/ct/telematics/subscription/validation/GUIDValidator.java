package com.tmna.ct.telematics.subscription.validation;

import com.tmna.ct.telematics.subscription.utils.ValidationRegexUtils;
import com.tmna.ct.telematics.subscription.validation.constraints.ValidGUID;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Pattern;

/**
 * This class is a custom validator to validate GUID.
 *
 * @author Srini Y
 * @version 1.0
 */

public class GUIDValidator implements ConstraintValidator<ValidGUID, String> {

  @Override
  public boolean isValid(String guid, ConstraintValidatorContext constraintValidatorContext) {
    return guid != null && Pattern.matches(ValidationRegexUtils.GUID_REGEX, guid);
  }

  @Override
  public void initialize(ValidGUID validGUID) {
    //Not used. Method Overridden
  }
}
