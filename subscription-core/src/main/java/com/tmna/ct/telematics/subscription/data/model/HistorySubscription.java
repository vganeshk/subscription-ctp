package com.tmna.ct.telematics.subscription.data.model;

public class HistorySubscription {

  private String vin;
  private String createDate;
  private Subscription subscription;

  /**
   * Gets vin.
   *
   * @return Value of vin.
   */
  public String getVin() {
    return vin;
  }

  /**
   * Gets subscription.
   *
   * @return Value of subscription.
   */
  public Subscription getSubscription() {
    return subscription;
  }

  /**
   * Gets createDate.
   *
   * @return Value of createDate.
   */
  public String getCreateDate() {
    return createDate;
  }

  /**
   * Sets new vin.
   *
   * @param vin New value of vin.
   */
  public void setVin(String vin) {
    this.vin = vin;
  }

  /**
   * Sets new createDate.
   *
   * @param createDate New value of createDate.
   */
  public void setCreateDate(String createDate) {
    this.createDate = createDate;
  }

  /**
   * Sets new subscription.
   *
   * @param subscription New value of subscription.
   */
  public void setSubscription(Subscription subscription) {
    this.subscription = subscription;
  }
}