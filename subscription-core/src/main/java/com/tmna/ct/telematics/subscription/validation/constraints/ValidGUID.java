package com.tmna.ct.telematics.subscription.validation.constraints;

import com.tmna.ct.telematics.subscription.validation.GUIDValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 */
@Target({ ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = GUIDValidator.class)
public @interface ValidGUID {

    String message() default "{CODE.INVALID.INCORRECT.INCORRECT.ATTRIBUTE}:{MESSAGE.INVALID.INCORRECT.GUID}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
