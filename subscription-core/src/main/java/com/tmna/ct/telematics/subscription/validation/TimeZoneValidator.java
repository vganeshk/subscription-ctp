package com.tmna.ct.telematics.subscription.validation;

import com.tmna.ct.telematics.subscription.utils.ValidationRegexUtils;
import com.tmna.ct.telematics.subscription.validation.constraints.ValidTimeZone;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Pattern;

/**
 * This class is a custom validator to validate TimeZone.
 *
 * @author Srini Y
 * @version 1.0
 */

public class TimeZoneValidator implements ConstraintValidator<ValidTimeZone, String> {

  @Override
  public boolean isValid(String timeZone, ConstraintValidatorContext constraintValidatorContext) {
    return timeZone != null && Pattern.matches(ValidationRegexUtils.TIME_ZONE_REGEX, timeZone);
  }

  @Override
  public void initialize(ValidTimeZone validTimeZone) {
    //Not used. Method Overridden
  }
}
