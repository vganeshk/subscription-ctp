package com.tmna.ct.telematics.subscription.data.model;

public class RemoteUser{
	private String remoteUserAuthDate;
	private String remoteUserGuid;
	private String createSource;
	private String updateDate;
	private String updateSource;
	private String remoteAuthChannel;
	private String createDate;

	public void setRemoteUserAuthDate(String remoteUserAuthDate){
		this.remoteUserAuthDate = remoteUserAuthDate;
	}

	public String getRemoteUserAuthDate(){
		return remoteUserAuthDate;
	}

	public void setRemoteUserGuid(String remoteUserGuid){
		this.remoteUserGuid = remoteUserGuid;
	}

	public String getRemoteUserGuid(){
		return remoteUserGuid;
	}

	public void setCreateSource(String createSource){
		this.createSource = createSource;
	}

	public String getCreateSource(){
		return createSource;
	}

	public void setUpdateDate(String updateDate){
		this.updateDate = updateDate;
	}

	public String getUpdateDate(){
		return updateDate;
	}

	public void setUpdateSource(String updateSource){
		this.updateSource = updateSource;
	}

	public String getUpdateSource(){
		return updateSource;
	}

	public void setRemoteAuthChannel(String remoteAuthChannel){
		this.remoteAuthChannel = remoteAuthChannel;
	}

	public String getRemoteAuthChannel(){
		return remoteAuthChannel;
	}

	public void setCreateDate(String createDate){
		this.createDate = createDate;
	}

	public String getCreateDate(){
		return createDate;
	}

	@Override
 	public String toString(){
		return 
			"RemoteUser{" + 
			"remoteUserAuthDate = '" + remoteUserAuthDate + '\'' + 
			",remoteUserGuid = '" + remoteUserGuid + '\'' + 
			",createSource = '" + createSource + '\'' + 
			",updateDate = '" + updateDate + '\'' + 
			",updateSource = '" + updateSource + '\'' + 
			",remoteAuthChannel = '" + remoteAuthChannel + '\'' + 
			",createDate = '" + createDate + '\'' + 
			"}";
		}
}
