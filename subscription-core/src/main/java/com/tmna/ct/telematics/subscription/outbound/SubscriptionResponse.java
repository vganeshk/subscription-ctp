package com.tmna.ct.telematics.subscription.outbound;

/**
 * Pojo representing subscriptionresponse
 *
 * @author Srini Y
 * @version 1.0
 */
public class SubscriptionResponse {

  private Status status;

  /**
   * Get the response status
   *
   * @return status
   */
  public Status getStatus() {
    return status;
  }

  /**
   * Set the response status
   *
   * @param status status
   */
  public void setStatus(Status status) {
    this.status = status;
  }
}
