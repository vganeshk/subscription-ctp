package com.tmna.ct.telematics.subscription.validation;

import com.tmna.ct.telematics.subscription.utils.ValidationRegexUtils;
import com.tmna.ct.telematics.subscription.validation.constraints.ValidUTCTimestamp;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Pattern;

/**
 * This class is a custom validator to validate UTCTimestamp.
 *
 * @author Srini Y
 * @version 1.0
 */

public class UTCTimestampValidator implements ConstraintValidator<ValidUTCTimestamp, String> {

  @Override
  public boolean isValid(String timestamp, ConstraintValidatorContext constraintValidatorContext) {
    return timestamp != null; /*&& Pattern.matches(ValidationRegexUtils.UTC_DATE_TIME_REGEX, timestamp)*/
  }

  @Override
  public void initialize(ValidUTCTimestamp validUTCTimestamp) {
    //Not used. Method Overridden
  }
}
