package com.tmna.ct.telematics.subscription.utils;

import com.tc.logging.ILogService;

/**
 * This class is used to define the utility methods related to logging.
 *
 * @author Srini Y
 * @version 1.0
 */
public class LogUtils {

  /**
   * Constructor for LogUtil
   */
  private LogUtils() {
    throw new IllegalStateException("LogUtils");
  }

  /**
   * This method is used to capture the entry point of a method.
   *
   * @param logger logger
   */
  public static void logMethodEntry(ILogService logger) {
    if (logger.isInfoEnabled()) {
      logger.info(Constants.METHOD_ENTRY);
    }
  }

  /**
   * This method is used to capture the exist point of a method.
   *
   * @param logger   logger
   * @param exitType exitType
   */
  public static void logMethodExit(ILogService logger, String exitType) {
    if (logger.isInfoEnabled()) {
      logger.info(exitType);
    }
  }


}
