package com.tmna.ct.telematics.subscription.validation;

import com.tmna.ct.telematics.subscription.utils.ValidationRegexUtils;
import com.tmna.ct.telematics.subscription.validation.constraints.ValidBrand;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Pattern;

/**
 * This class is a custom validator to validate Brand.
 *
 * @author Srini Y
 * @version 1.0
 */

public class BrandValidator implements ConstraintValidator<ValidBrand, String> {

  @Override
  public boolean isValid(String brand, ConstraintValidatorContext constraintValidatorContext) {
    return brand == null || Pattern.matches(ValidationRegexUtils.BRAND_REGEX, brand);
  }

  @Override
  public void initialize(ValidBrand validBrand) {
    //Not used. Method Overridden
  }
}
