package com.tmna.ct.telematics.subscription.outbound;

/**
 * Pojo class representing the Message class in response
 *
 * @author Srini Y
 * @version 1.0
 */
public class Message {

  private String description;

  private String responseCode;

  private String detailedDescription;

  /**
   * Get the error description.
   *
   * @return description
   */
  public String getDescription() {
    return description;
  }

  /**
   * Set the error description
   *
   * @param description description
   */
  public void setDescription(String description) {
    this.description = description;
  }

  /**
   * get the response codes from message.
   *
   * @return responseCode
   */
  public String getResponseCode() {
    return responseCode;
  }

  /**
   * Set the response code in message
   *
   * @param responseCode responseCode
   */
  public void setResponseCode(String responseCode) {
    this.responseCode = responseCode;
  }

  /**
   * Get the detailed description
   *
   * @return detailedDescription
   */
  public String getDetailedDescription() {
    return detailedDescription;
  }

  /**
   * Set the detailed description
   *
   * @param detailedDescription detailedDescription
   */
  public void setDetailedDescription(String detailedDescription) {
    this.detailedDescription = detailedDescription;
  }
}
