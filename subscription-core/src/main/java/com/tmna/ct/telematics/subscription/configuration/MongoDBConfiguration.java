package com.tmna.ct.telematics.subscription.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Encapsulates mongo db configuration.
 *
 * @author Srini Y
 * @version 1.0
 */
@Configuration
public class MongoDBConfiguration {

  @Value("${ct.telematics.mongodb.subscription.collection.name:subscription}")
  private String subscriptionCollectionName;

  @Value("${ct.telematics.mongodb.subscription.history.collection.name:subscription-history}")
  private String historySubscriptionCollectionName;

  @Bean
  public String subscriptionCollectionName() {
    return subscriptionCollectionName;
  }

  @Bean
  public String historySubscriptionCollectionName() {
    return historySubscriptionCollectionName;
  }
}
