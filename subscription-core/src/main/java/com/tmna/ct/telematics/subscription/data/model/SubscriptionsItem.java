package com.tmna.ct.telematics.subscription.data.model;

public class SubscriptionsItem {
  private String subscriptionID;
  private String zuoraSubscriptionID;
  private String productID;
  private String ratePlanID;
  private String packageID;
  private String packagePrice;
  private String packageCurrency;
  private String subscriptionStatus;
  private String subscriptionStartDate;
  private String subscriptionEndDate;

  /**
   * Gets ratePlanID.
   *
   * @return Value of ratePlanID.
   */
  public String getRatePlanID() {
    return ratePlanID;
  }

  /**
   * Sets new ratePlanID.
   *
   * @param ratePlanID New value of ratePlanID.
   */
  public void setRatePlanID(String ratePlanID) {
    this.ratePlanID = ratePlanID;
  }

  /**
   * Gets packageCurrency.
   *
   * @return Value of packageCurrency.
   */
  public String getPackageCurrency() {
    return packageCurrency;
  }

  /**
   * Sets new packageCurrency.
   *
   * @param packageCurrency New value of packageCurrency.
   */
  public void setPackageCurrency(String packageCurrency) {
    this.packageCurrency = packageCurrency;
  }

  /**
   * Gets packagePrice.
   *
   * @return Value of packagePrice.
   */
  public String getPackagePrice() {
    return packagePrice;
  }

  /**
   * Sets new packagePrice.
   *
   * @param packagePrice New value of packagePrice.
   */
  public void setPackagePrice(String packagePrice) {
    this.packagePrice = packagePrice;
  }

  /**
   * Gets packageID.
   *
   * @return Value of packageID.
   */
  public String getPackageID() {
    return packageID;
  }

  /**
   * Sets new packageID.
   *
   * @param packageID New value of packageID.
   */
  public void setPackageID(String packageID) {
    this.packageID = packageID;
  }

  /**
   * Gets subscriptionStatus.
   *
   * @return Value of subscriptionStatus.
   */
  public String getSubscriptionStatus() {
    return subscriptionStatus;
  }

  /**
   * Sets new subscriptionStatus.
   *
   * @param subscriptionStatus New value of subscriptionStatus.
   */
  public void setSubscriptionStatus(String subscriptionStatus) {
    this.subscriptionStatus = subscriptionStatus;
  }

  /**
   * Gets productID.
   *
   * @return Value of productID.
   */
  public String getProductID() {
    return productID;
  }

  /**
   * Sets new productID.
   *
   * @param productID New value of productID.
   */
  public void setProductID(String productID) {
    this.productID = productID;
  }

  /**
   * Gets zuoraSubscriptionID.
   *
   * @return Value of zuoraSubscriptionID.
   */
  public String getZuoraSubscriptionID() {
    return zuoraSubscriptionID;
  }

  /**
   * Sets new zuoraSubscriptionID.
   *
   * @param zuoraSubscriptionID New value of zuoraSubscriptionID.
   */
  public void setZuoraSubscriptionID(String zuoraSubscriptionID) {
    this.zuoraSubscriptionID = zuoraSubscriptionID;
  }


  /**
   * Gets subscriptionID.
   *
   * @return Value of subscriptionID.
   */
  public String getSubscriptionID() {
    return subscriptionID;
  }

  /**
   * Sets new subscriptionID.
   *
   * @param subscriptionID New value of subscriptionID.
   */
  public void setSubscriptionID(String subscriptionID) {
    this.subscriptionID = subscriptionID;
  }

  /**
   * Gets subscriptionStartDate.
   *
   * @return Value of subscriptionStartDate.
   */
  public String getSubscriptionStartDate() {
    return subscriptionStartDate;
  }

  /**
   * Sets new subscriptionStartDate.
   *
   * @param subscriptionStartDate New value of subscriptionStartDate.
   */
  public void setSubscriptionStartDate(String subscriptionStartDate) {
    this.subscriptionStartDate = subscriptionStartDate;
  }

  /**
   * Gets subscriptionEndDate.
   *
   * @return Value of subscriptionEndDate.
   */
  public String getSubscriptionEndDate() {
    return subscriptionEndDate;
  }

  /**
   * Sets new subscriptionEndDate.
   *
   * @param subscriptionEndDate New value of subscriptionEndDate.
   */
  public void setSubscriptionEndDate(String subscriptionEndDate) {
    this.subscriptionEndDate = subscriptionEndDate;
  }

  @Override
  public String toString() {
    return
        "SubscriptionsItem{" +
            "productID = '" + productID + '\'' +
            ",ratePlanID = '" + ratePlanID + '\'' +
            ",packageID = '" + packageID + '\'' +
            ",packagePrice = '" + packagePrice + '\'' +
            ",subscriptionStatus = '" + subscriptionStatus + '\'' +
            ",packageCurrency = '" + packageCurrency + '\'' +
            ",subscriptionID = '" + subscriptionID + '\'' +
            ",zuoraSubscriptionID = '" + zuoraSubscriptionID + '\'' +
            ",subscriptionStartDate = '" + subscriptionStartDate + '\'' +
            "}";
  }
}
