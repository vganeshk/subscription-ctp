package com.tmna.ct.telematics.subscription.utils;

/**
 * This class is used to define the RegEx for subscription domain validation.
 *
 * @author Srini Y
 * @version 1.0
 */
public class ValidationRegexUtils {

  /**
   * Mandatory for RS. It could be of maximum 32 characters and can contain special characters such as =, + or /.
   */
  public static final String GUID_REGEX = "^[A-Za-z0-9\\s\\-\\_\\.\\#\\@\\=\\+\\%]{32}$";
  /**
   * Optional. Possible values are Full or None.
   */
  public static final String OPT_IN_OUT_REGEX = "^(Full|None)$";
  /**
   * Optional. Possible values are either 01, 02, T1 or T2.
   */
  public static final String RS_MAIL_SUB_TYPE_REGEX = "^(01|03|T1|T2)$";
  /**
   * Optional. Possible values are 15.
   */
  public static final String RS_MAIL_TYPE_REGEX = "^(15)$";
  /**
   * Optional. Possible values are 16.
   */
  public static final String RD_MAIL_TYPE_REGEX = "^(16)$";
  /**
   * Optional. Possible values are 19.
   */
  public static final String VP_MAIL_TYPE_REGEX = "^(19)$";
  /**
   * Optional. Possible values are ACK, PENDING, ACTIVE, INACTIVE, FAILED.
   */
  public static final String STATUS_REGEX = "^(ACK|PENDING|ACTIVE|INACTIVE|FAILED)$";
  /**
   * Optional. Possible values are ACK, PENDING, ACTIVE, INACTIVE, FAILED.
   */
  public static final String TSP_STATUS_REGEX = "^(ON|OFF)$";
  /**
   * Optional. Possible values are ACK or SUCCESS.
   */
  public static final String TSP_FINAL_STATUS_REGEX = "^(ACK|SUCCESS)$";
  /**
   * Optional. Possible values are SPEC_I, ADMIN_GET_STATUS_SYNC_TSC.
   */
  public static final String RS_UPDATE_SOURCE_REGEX = "^(TSC|SPEC_I|ADMIN_GET_STATUS_SYNC_TSC)$";
  /**
   * Mandatory. Should be 17 Characters.
   */
  public static final String VIN_REGEX = "^[0-9A-HJ-NPR-Z]{17}$";
  /**
   * Optional
   */
  public static final String UTC_DATE_TIME_REGEX = "^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}.[0-9]{3}[+-][0-9]{4}$";
  /**
   *
   */
  public static final String BRAND_REGEX = "^(Lexus|Toyota)$";
  /**
   *
   */
  public static final String UNIT_INTERVAL_REGEX = "^(minutes|days|once|sec|hr)$";
  /**
   *
   */
  public static final String LANGUAGE_REGEX = "^(en-US|es-US|fr-CA)$";
  /**
   *
   */
  public static final String PHONE_NUMBER_REGEX = "^((\\+1)?[\\s-]?)?\\(?[2-9]\\d\\d\\)?[\\s-]?[2-9]\\d\\d[\\s-]?\\d\\d\\d\\d$";
  /**
   *
   */
  public static final String TIME_ZONE_REGEX = "^([1-7]|[1][1-9]|[2][0-1]|[3][1-5])$";
  /**
   * Optional. Possible values are SPEC_I, ADMIN_GET_STATUS_SYNC_TSC.
   */
  public static final String TSP_UPDATE_SOURCE_REGEX = "^(TC|ADMIN_GET_STATUS_SYNC_TC)$";
  /**
   * Mandatory. Should be application/json.
   */
  public static final String CONTENT_TYPE_REGEX = "^(application/json)$";
  /**
   * Mandatory. Possible values are L, T.
   */
  public static final String X_BRAND_REGEX = "^(L|T)$";
  /**
   * Mandatory. Possible values are TC, LER, TRC, TARP_SELF, TARP_DEALER, TARP_AGENT, ADMIN_TOOL.
   */
  public static final String X_CHANNEL_REGEX = "^(TC|TSC|LER|TRC|TARP_SELF|TARP_DEALER|TARP_AGENT|ADMIN_TOOL)$";
  /**
   * Mandatory. Should be 36 Character.
   */
  public static final String X_CORRELATION_ID_REGEX = "^[a-zA-Z0-9-]{36}$";
  /**
   * Mandatory. Should be Deflate.
   */
  public static final String X_ACCEPT_ENCODING_REGEX = "(deflate)";
  /**
   * Mandatory.
   */
  public static final String DATETIME_REGEX = "^[0-9]{13}$";

  private ValidationRegexUtils() {
    throw new IllegalStateException("ValidationRegexUtils");
  }
}
