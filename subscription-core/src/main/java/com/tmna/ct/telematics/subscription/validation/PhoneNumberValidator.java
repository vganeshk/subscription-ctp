package com.tmna.ct.telematics.subscription.validation;

import com.tmna.ct.telematics.subscription.utils.ValidationRegexUtils;
import com.tmna.ct.telematics.subscription.validation.constraints.ValidPhoneNumber;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Pattern;

/**
 * This class is a custom validator to validate PhoneNumber.
 *
 * @author Srini Y
 * @version 1.0
 */

public class PhoneNumberValidator implements ConstraintValidator<ValidPhoneNumber, Long> {

  @Override
  public boolean isValid(Long phoneNumber, ConstraintValidatorContext constraintValidatorContext) {
    return phoneNumber != null && Pattern.matches(ValidationRegexUtils.PHONE_NUMBER_REGEX, phoneNumber.toString());
  }

  @Override
  public void initialize(ValidPhoneNumber validPhoneNumber) {
    //Not used. Method Overridden
  }

}
