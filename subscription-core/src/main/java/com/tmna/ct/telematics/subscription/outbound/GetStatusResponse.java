package com.tmna.ct.telematics.subscription.outbound;

/**
 * POJO Representing the GetStatusRequest.
 *
 * @author Srini Y
 * @version 1.0
 */
public class GetStatusResponse {

  private Payload payload;

  private Status status;

  /**
   * Get the response payload
   *
   * @return payload
   */
  public Payload getPayload() {
    return payload;
  }

  /**
   * Set the response payload
   *
   * @param payload payload
   */
  public void setPayload(Payload payload) {
    this.payload = payload;
  }

  /**
   * Get the response status
   *
   * @return status
   */
  public Status getStatus() {
    return status;
  }

  /**
   * Set the status
   *
   * @param status status
   */
  public void setStatus(Status status) {
    this.status = status;
  }
}
