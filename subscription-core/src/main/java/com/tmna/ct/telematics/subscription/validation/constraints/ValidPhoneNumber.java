package com.tmna.ct.telematics.subscription.validation.constraints;

import com.tmna.ct.telematics.subscription.validation.PhoneNumberValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 */
@Target({ ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = PhoneNumberValidator.class)
public @interface ValidPhoneNumber {

    String message() default "{CODE.INVALID.INCORRECT.SUBSCRIPTION.ATTRIBUTE}:{MESSAGE.INVALID.SUBSCRIPTION.PHONE_NUMBER}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
