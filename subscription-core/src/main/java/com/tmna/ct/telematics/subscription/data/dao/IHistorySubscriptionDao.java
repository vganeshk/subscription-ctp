package com.tmna.ct.telematics.subscription.data.dao;

import com.tmna.ct.telematics.subscription.data.model.HistorySubscription;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * This interface is used to perform DB operations on 'subscription' collection
 *
 * @author Srini Y
 * @version 1.0
 */
public interface IHistorySubscriptionDao extends MongoRepository<HistorySubscription, String> {

 /**
 * This method is used to save/insert a new customer
 *
 * @param historySubscription Incoming new subscription object
 * @return Saved subscription object
 */
  HistorySubscription save(HistorySubscription historySubscription);

  /**
 * This method is used to fetch subscription based on VIN & sorting using modified timestamp
 *
 * @param vin  VIN to be searched
 * @param sort Sort criteria
 * @return List of subscription
 */
  Page<HistorySubscription> findAllByVin(String vin, Pageable pageable);

}

