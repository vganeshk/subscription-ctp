package com.tmna.ct.telematics.subscription.data.dao;

import com.tmna.ct.telematics.subscription.data.model.Subscription;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * This interface is used to perform DB operations on 'subscription' collection
 *
 * @author Srini Y
 * @version 1.0
 */
public interface ISubscriptionDao extends MongoRepository<Subscription, String> {

  /**
   * This method is used to fetch subscription based on VIN & sorting using modified timestamp
   *
   * @param vin  VIN to be searched
   * @return List of subscription
   */
  Subscription findByVin(String vin);

  /**
   * This method is used to save/insert a new customer
   *
   * @param subscription new subscriptionobject
   * @return Saved subscription object
   */
  Subscription save(Subscription subscription);

    /**
     * This method is used to check if a record for VIN exist
     *
     * @param vin  VIN to be searched
     * @return boolean value as result
     */
    boolean existsByVin(String vin);
}

