package com.tmna.ct.telematics.subscription.validation;

import com.tmna.ct.telematics.subscription.utils.ValidationRegexUtils;
import com.tmna.ct.telematics.subscription.validation.constraints.ValidUpdateSource;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Pattern;

/**
 * This class is a custom validator to validate UpdateSource.
 * @author Srini Y
 * @version 1.0
 */

public class UpdateSourceValidator implements ConstraintValidator<ValidUpdateSource, String> {

    @Override
    public boolean isValid(String updateSource, ConstraintValidatorContext constraintValidatorContext) {
        return updateSource != null && Pattern.matches(ValidationRegexUtils.RS_UPDATE_SOURCE_REGEX,updateSource);
    }

    @Override
    public void initialize(ValidUpdateSource validUpdateSource) {
        //Not used. Method Overridden
    }
}
