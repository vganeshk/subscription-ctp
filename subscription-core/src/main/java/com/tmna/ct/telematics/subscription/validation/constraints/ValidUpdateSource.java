package com.tmna.ct.telematics.subscription.validation.constraints;

import com.tmna.ct.telematics.subscription.validation.UpdateSourceValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 */
@Target({ ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = UpdateSourceValidator.class)
public @interface ValidUpdateSource {

    String message() default "{CODE.INVALID.INCORRECT.SUBSCRIPTION.ATTRIBUTE}:{MESSAGE.INVALID.SUBSCRIPTION.UPDATE.SOURCE}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
