package com.tmna.ct.telematics.subscription.utils;

import com.tc.logging.ILogService;
import com.tc.logging.LogFactory;
import com.tc.logging.annotation.LoggingMetadata;
import com.tmna.ct.telematics.subscription.data.model.HistorySubscription;
import com.tmna.ct.telematics.subscription.data.model.Subscription;
import com.tmna.ct.telematics.subscription.outbound.*;
import org.springframework.http.HttpStatus;

import javax.validation.ConstraintViolation;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * This class is used to build the service response for Subscription micro services.
 *
 * @author Srini Y
 * @version 1.0
 */
@LoggingMetadata(feature = "ResponseUtils")
public class ResponseUtils {

  private static final ILogService logger = LogFactory.getLogger(ResponseUtils.class);

  /**
   * This method is used to build Response message for Subscription CRUD operations
   *
   * @param subscription Subscription object
   * @return subscriptionResponse object with payload & status
   */
  @LoggingMetadata(goal = "buildSubscriptionResponse")
  public SubscriptionResponse buildSubscriptionResponse(Subscription subscription ) {
    LogUtils.logMethodEntry(logger);
    SubscriptionResponse subscriptionResponse = new SubscriptionResponse();
    subscriptionResponse.setStatus(this.buildSuccessStatus());
    LogUtils.logMethodExit(logger, Constants.METHOD_EXIT_SUCCESSFUL);
    return subscriptionResponse;
  }

  /**
   * This method is used to build Response message for Subscription CRUD operations
   *
   * @param dbSubscription Subscription object from DB.
   * @return getStatusResponse object with payload & status
   */
  @LoggingMetadata(goal = "buildGetStatusSuccessResponse")
  public GetStatusResponse buildGetStatusSuccessResponse(Subscription dbSubscription) {
    LogUtils.logMethodEntry(logger);
    GetStatusResponse getStatusResponse = new GetStatusResponse();
    getStatusResponse.setStatus(this.buildSuccessStatus());
    Payload payload = new Payload();
    payload.setSubscription(dbSubscription);
    getStatusResponse.setPayload(payload);
    LogUtils.logMethodExit(logger, Constants.METHOD_EXIT_SUCCESSFUL);
    return getStatusResponse;
  }

  /**
   * This method is used to build Response message for Subscription CRUD operations
   *
   * @param dbSubscription Subscription object from DB.
   * @return getStatusResponse object with payload & status
   */
  @LoggingMetadata(goal = "buildGetStatusSuccessResponse")
  public GetStatusResponse buildHistorySubscriptionSuccessResponse(List<HistorySubscription> historySubscription, Long totalElements, int totalPage) {
    LogUtils.logMethodEntry(logger);
    GetStatusResponse getStatusResponse = new GetStatusResponse();
    getStatusResponse.setStatus(this.buildSuccessStatus());
    Payload payload = new Payload();
    List<Subscription> subscriptionList = new ArrayList<>();
    if(historySubscription != null){
      historySubscription.forEach(p -> subscriptionList.add(p.getSubscription()));
    }
    payload.setSubscriptionHistory(subscriptionList);
    payload.setPage(totalPage);
    payload.setCount(totalElements);
    getStatusResponse.setPayload(payload);
    LogUtils.logMethodExit(logger, Constants.METHOD_EXIT_SUCCESSFUL);
    return getStatusResponse;
  }

  /**
   * This method is used to build the Status part of the subscriptionresponse object.
   *
   * @return status status
   */
  @LoggingMetadata(goal = "buildSuccessStatus")
  private Status buildSuccessStatus() {
    LogUtils.logMethodEntry(logger);
    Status status = new Status();
    status.setHttpCode(HttpStatus.OK);
    status.setMessages(this.buildSuccessMessage());
    LogUtils.logMethodExit(logger, Constants.METHOD_EXIT_SUCCESSFUL);
    return status;
  }

  /**
   * This method is used to build the status part of the error response.
   *
   * @param violationSet      List of business rule violations
   * @param headerMessageList List of header rule violations
   * @return status
   */
  @LoggingMetadata(goal = "buildErrorStatus")
  private Status buildErrorStatus(Set<ConstraintViolation<Subscription>> violationSet,
                                  List<Message> headerMessageList) {
    LogUtils.logMethodEntry(logger);
    Status status = new Status();
    status.setHttpCode(HttpStatus.BAD_REQUEST);
    status.setMessages(this.buildErrorMessage(violationSet, headerMessageList));
    LogUtils.logMethodExit(logger, Constants.METHOD_EXIT_SUCCESSFUL);
    return status;
  }

  /**
   * This method is used to build the status part of the error response.
   *
   * @param headerMessageList List of header rule violations
   * @return status
   */
  @LoggingMetadata(goal = "buildErrorStatus")
  private Status buildErrorStatus(List<Message> headerMessageList) {
    LogUtils.logMethodEntry(logger);
    Status status = new Status();
    status.setHttpCode(HttpStatus.BAD_REQUEST);
    status.setMessages(this.buildErrorMessage(headerMessageList));
    LogUtils.logMethodExit(logger, Constants.METHOD_EXIT_SUCCESSFUL);
    return status;
  }

  /**
   * This method is used to build the Message part of the response status
   *
   * @return List of response messages.
   */
  @LoggingMetadata(goal = "buildSuccessMessage")
  private List<Message> buildSuccessMessage() {
    LogUtils.logMethodEntry(logger);
    List<Message> messageList = new ArrayList<>();
    Message message = new Message();
    message.setResponseCode(Constants.SUBSCRIPTION_CREATION_CODE);
    message.setDetailedDescription(Constants.SUBSCRIPTION_CREATED_SUCCESSFULLY);
    message.setDescription(Constants.REQUEST_EXECUTED_SUCCESSFULLY);
    messageList.add(message);
    LogUtils.logMethodExit(logger, Constants.METHOD_EXIT_SUCCESSFUL);
    return messageList;
  }

  /**
   * This method is used to build error message as part of business exception.
   *
   * @param violationSet      List of business rule violations
   * @param headerMessageList List of header rule violations
   * @return list of messages
   */
  @LoggingMetadata(goal = "buildErrorMessage")
  private List<Message> buildErrorMessage(Set<ConstraintViolation<Subscription>> violationSet,
                                          List<Message> headerMessageList) {
    LogUtils.logMethodEntry(logger);
    List<Message> messageList = new ArrayList<>();
    if(violationSet != null) {
      violationSet.forEach(c ->
      {
        String[] errorDetails = c.getMessage().split(":");
        Message message = new Message();
        message.setResponseCode(errorDetails[0]);
        message.setDetailedDescription(errorDetails[1]);
        message.setDescription(Constants.INCORRECT_INVALID_BODY_ATTRIBUTES);
        messageList.add(message);
      });
    }
    messageList.addAll(headerMessageList);
    LogUtils.logMethodExit(logger, Constants.METHOD_EXIT_SUCCESSFUL);
    return messageList;
  }

  /**
   * This method is used to build error message as part of business exception.
   *
   * @param headerMessageList List of header rule violations
   * @return list of messages
   */
  @LoggingMetadata(goal = "buildErrorMessage")
  private List<Message> buildErrorMessage(List<Message> headerMessageList) {
    LogUtils.logMethodEntry(logger);
    List<Message> messageList = new ArrayList<>();
    messageList.addAll(headerMessageList);
    LogUtils.logMethodExit(logger, Constants.METHOD_EXIT_SUCCESSFUL);
    return messageList;
  }


  /**
   * This message is used to build a customer response during a business exception
   *
   * @param violationSet      List of business rule violations
   * @param headerMessageList List of header rule violations
   * @return subscriptionResponse
   */
  @LoggingMetadata(goal = "buildCustomerResponse")
  public SubscriptionResponse buildSubscriptionResponse(Set<ConstraintViolation<Subscription>> violationSet,
                                                        List<Message> headerMessageList) {
    LogUtils.logMethodEntry(logger);
    SubscriptionResponse subscriptionResponse = new SubscriptionResponse();
    subscriptionResponse.setStatus(this.buildErrorStatus(violationSet, headerMessageList));
    LogUtils.logMethodExit(logger, Constants.METHOD_EXIT_SUCCESSFUL);
    return subscriptionResponse;
  }

  /**
   * This message is used to build a customer response during a business exception
   *
   * @param headerMessageList List of header rule violations
   * @return customerresponse
   */
  @LoggingMetadata(goal = "buildGetStatusResponse")
  public GetStatusResponse buildGetStatusResponse(List<Message> headerMessageList) {
    LogUtils.logMethodEntry(logger);
    GetStatusResponse getStatusResponse = new GetStatusResponse();
    getStatusResponse.setStatus(this.buildErrorStatus(headerMessageList));
    LogUtils.logMethodExit(logger, Constants.METHOD_EXIT_SUCCESSFUL);
    return getStatusResponse;
  }

  /**
   * This method is used to build a error response during system exception
   *
   * @return status
   */
  @LoggingMetadata(goal = "buildCustomerErrorResponse")
  public Status buildSubscriptionErrorResponse() {
    LogUtils.logMethodEntry(logger);
    Status status = new Status();
    Message message = new Message();

    message.setResponseCode(Constants.INTERNAL_SERVER_ERROR_CODE);
    message.setDescription(Constants.INTERNAL_SERVER_ERROR);
    message.setDetailedDescription(Constants.INTERNAL_SERVER_ERROR);

    List<Message> messageList = new ArrayList<>();
    messageList.add(message);
    status.setMessages(messageList);
    LogUtils.logMethodExit(logger, Constants.METHOD_EXIT_SUCCESSFUL);
    return status;
  }

  /**
   * this method is used to build a 404 response when a VIN or correlation ID is not found in DB
   * during a GET operation
   *
   * @param notFoundAttribute VIN or Correlation ID
   * @return Status object
   */
  @LoggingMetadata(goal = "buildNotFoundResponse")
  private Status buildNotFoundResponse(String notFoundAttribute) {
    LogUtils.logMethodEntry(logger);
    Status status = new Status();
    Message message = new Message();

    if(Constants.VIN.equalsIgnoreCase(notFoundAttribute)) {
      message.setResponseCode(Constants.VIN_NOT_FOUND_CODE);
      message.setDescription(Constants.VIN_NOT_FOUND);
      message.setDetailedDescription(Constants.VIN_NOT_FOUND);
    }else if(Constants.CORRELATION_ID.equalsIgnoreCase(notFoundAttribute)){
      message.setResponseCode(Constants.CORRELATION_ID_NOT_FOUND_CODE);
      message.setDescription(Constants.CORRELATIOND_ID_NOT_FOUND);
      message.setDetailedDescription(Constants.CORRELATIOND_ID_NOT_FOUND);
    }

    List<Message> messageList = new ArrayList();
    messageList.add(message);
    status.setMessages(messageList);
    status.setHttpCode(HttpStatus.NOT_FOUND);
    LogUtils.logMethodExit(logger, Constants.METHOD_EXIT_SUCCESSFUL);
    return status;
  }

  /**
   * this method is used to build a 404 response when a VIN or correlation ID is not found in DB
   * during a GET operation
   *
   * @param notFoundAttribute VIN or Correlation ID
   * @return GetStatusResponse object
   */
  @LoggingMetadata(goal = "buildGetStatusNotFoundResponse")
  public GetStatusResponse buildGetStatusNotFoundResponse(String notFoundAttribute) {
    LogUtils.logMethodEntry(logger);
    GetStatusResponse getStatusResponse = new GetStatusResponse();
    getStatusResponse.setStatus(this.buildNotFoundResponse(notFoundAttribute));
    LogUtils.logMethodExit(logger, Constants.METHOD_EXIT_SUCCESSFUL);
    return getStatusResponse;
  }

  /**
   * This message is used to build a Subscription response during a business exception
   *
   * @param headerMessageList List of header rule violations
   * @return SubscriptionResponse
   */
  @LoggingMetadata(goal = "buildSubscriptionErrorResponse")
  public SubscriptionResponse buildSubscriptionErrorResponse(List<Message> headerMessageList) {
    LogUtils.logMethodEntry(logger);
    SubscriptionResponse subscriptionResponse = new SubscriptionResponse();
    subscriptionResponse.setStatus(this.buildErrorStatus(headerMessageList));
    LogUtils.logMethodExit(logger, Constants.METHOD_EXIT_SUCCESSFUL);
    return subscriptionResponse;
  }

  /**
   * This message is used to build a Subscription response when a correlation ID not found.
   *
   * @param notFoundAttribute Correlation ID
   * @return SubscriptionResponse
   */
  @LoggingMetadata(goal = "buildCIDNotFoundResponse")
  public SubscriptionResponse buildCIDNotFoundResponse(String notFoundAttribute) {
    LogUtils.logMethodEntry(logger);
    SubscriptionResponse subscriptionResponse = new SubscriptionResponse();
    subscriptionResponse.setStatus(this.buildNotFoundResponse(notFoundAttribute));
    LogUtils.logMethodExit(logger, Constants.METHOD_EXIT_SUCCESSFUL);
    return subscriptionResponse;
  }

  /**
   *
   * @param responseCode
   * @param description
   * @param detailedDescription
   * @return
   */
  public Message buildMessage(String responseCode, String description, String detailedDescription){
    LogUtils.logMethodEntry(logger);
    Message message = new Message();
    message.setResponseCode(responseCode);
    message.setDescription(description);
    message.setDetailedDescription(detailedDescription);
    return message;
  }
}
