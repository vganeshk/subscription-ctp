package com.tmna.ct.telematics.subscription.configuration;

import com.tc.logging.webmvc.interceptor.LoggingContextInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * This class is used to define a interceptor for adding a GUID in logging statement.
 *
 * @author Srini Y
 * @version 1.0
 */
@Configuration
public class LoggerConfiguration extends WebMvcConfigurerAdapter {

  /**
   * Adds LoggingContextInterceptor to interceptor registry
   *
   * @param registry Added to the {@link InterceptorRegistry}.
   */
  @Override
  public void addInterceptors(InterceptorRegistry registry) {
    registry.addInterceptor(new LoggingContextInterceptor());
  }
}
