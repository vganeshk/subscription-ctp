package com.tmna.ct.telematics.subscription.validation.constraints;

import com.tmna.ct.telematics.subscription.validation.BrandValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 */
@Target({ ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = BrandValidator.class)
public @interface ValidBrand {

    String message() default "{CODE.INVALID.INCORRECT.INCORRECT.ATTRIBUTE}:{MESSAGE.INVALID.INCORRECT.BRAND}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
