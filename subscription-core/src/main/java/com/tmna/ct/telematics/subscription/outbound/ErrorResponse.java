package com.tmna.ct.telematics.subscription.outbound;

/**
 * Class to build a response object incase of system exception
 *
 * @author Srini Y
 * @version 1.0
 */
public class ErrorResponse {

  private Status status;

  /**
   * Constructor with status variable
   *
   * @param status
   */
  public ErrorResponse(Status status) {
    this.status = status;
  }

  /**
   * Get the status from error response
   *
   * @return status
   */
  public Status getStatus() {
    return status;
  }

}
