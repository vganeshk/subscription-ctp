package com.tmna.ct.telematics.subscription.validation;

import com.tmna.ct.telematics.subscription.utils.ValidationRegexUtils;
import com.tmna.ct.telematics.subscription.validation.constraints.ValidStatus;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Pattern;

/**
 * This class is a custom validator to validate Status.
 * @author Srini Y
 * @version 1.0
 */

public class StatusValidator implements ConstraintValidator<ValidStatus, String> {

    @Override
    public boolean isValid(String status, ConstraintValidatorContext constraintValidatorContext) {
        return status != null && Pattern.matches(ValidationRegexUtils.STATUS_REGEX,status);
    }

    @Override
    public void initialize(ValidStatus validStatus) {
        //Not used. Method Overridden
    }
}
