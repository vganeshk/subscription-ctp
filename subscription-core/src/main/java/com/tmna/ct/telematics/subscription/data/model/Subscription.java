package com.tmna.ct.telematics.subscription.data.model;

import java.util.List;

public class Subscription {
  private String vin;
  private String region;
  private String generation;
  private String contractID;
  private RemoteUser remoteUser;
  private String subscriberGuid;
  private String emergencyContactName;
  private String emergencyContactNumber;
  private List<SubscriptionsItem> subscriptions;
  private DataConsent dataConsent;
  private String dofu;
  private String termsAccecptanceDate;
  private String waiver;
  private String createDate;
  private String createSource;
  private String updateDate;
  private String updateSource;


  /**
   * Gets contractID.
   *
   * @return Value of contractID.
   */
  public String getContractID() {
    return contractID;
  }

  /**
   * Sets new contractID.
   *
   * @param contractID New value of contractID.
   */
  public void setContractID(String contractID) {
    this.contractID = contractID;
  }

  /**
   * Gets termsAccecptanceDate.
   *
   * @return Value of termsAccecptanceDate.
   */
  public String getTermsAccecptanceDate() {
    return termsAccecptanceDate;
  }

  /**
   * Sets new termsAccecptanceDate.
   *
   * @param termsAccecptanceDate New value of termsAccecptanceDate.
   */
  public void setTermsAccecptanceDate(String termsAccecptanceDate) {
    this.termsAccecptanceDate = termsAccecptanceDate;
  }

  /**
   * Gets emergencyContactNumber.
   *
   * @return Value of emergencyContactNumber.
   */
  public String getEmergencyContactNumber() {
    return emergencyContactNumber;
  }

  /**
   * Sets new emergencyContactNumber.
   *
   * @param emergencyContactNumber New value of emergencyContactNumber.
   */
  public void setEmergencyContactNumber(String emergencyContactNumber) {
    this.emergencyContactNumber = emergencyContactNumber;
  }

  /**
   * Gets vin.
   *
   * @return Value of vin.
   */
  public String getVin() {
    return vin;
  }

  /**
   * Sets new vin.
   *
   * @param vin New value of vin.
   */
  public void setVin(String vin) {
    this.vin = vin;
  }

  /**
   * Gets updateSource.
   *
   * @return Value of updateSource.
   */
  public String getUpdateSource() {
    return updateSource;
  }

  /**
   * Sets new updateSource.
   *
   * @param updateSource New value of updateSource.
   */
  public void setUpdateSource(String updateSource) {
    this.updateSource = updateSource;
  }

  /**
   * Gets createDate.
   *
   * @return Value of createDate.
   */
  public String getCreateDate() {
    return createDate;
  }

  /**
   * Sets new createDate.
   *
   * @param createDate New value of createDate.
   */
  public void setCreateDate(String createDate) {
    this.createDate = createDate;
  }

  /**
   * Gets dofu.
   *
   * @return Value of dofu.
   */
  public String getDofu() {
    return dofu;
  }

  /**
   * Sets new dofu.
   *
   * @param dofu New value of dofu.
   */
  public void setDofu(String dofu) {
    this.dofu = dofu;
  }

  /**
   * Gets region.
   *
   * @return Value of region.
   */
  public String getRegion() {
    return region;
  }

  /**
   * Sets new region.
   *
   * @param region New value of region.
   */
  public void setRegion(String region) {
    this.region = region;
  }

  /**
   * Gets subscriptions.
   *
   * @return Value of subscriptions.
   */
  public List<SubscriptionsItem> getSubscriptions() {
    return subscriptions;
  }

  /**
   * Sets new subscriptions.
   *
   * @param subscriptions New value of subscriptions.
   */
  public void setSubscriptions(List<SubscriptionsItem> subscriptions) {
    this.subscriptions = subscriptions;
  }

  /**
   * Gets generation.
   *
   * @return Value of generation.
   */
  public String getGeneration() {
    return generation;
  }

  /**
   * Sets new generation.
   *
   * @param generation New value of generation.
   */
  public void setGeneration(String generation) {
    this.generation = generation;
  }

  /**
   * Gets createSource.
   *
   * @return Value of createSource.
   */
  public String getCreateSource() {
    return createSource;
  }

  /**
   * Sets new createSource.
   *
   * @param createSource New value of createSource.
   */
  public void setCreateSource(String createSource) {
    this.createSource = createSource;
  }

  /**
   * Gets remoteUser.
   *
   * @return Value of remoteUser.
   */
  public RemoteUser getRemoteUser() {
    return remoteUser;
  }

  /**
   * Sets new remoteUser.
   *
   * @param remoteUser New value of remoteUser.
   */
  public void setRemoteUser(RemoteUser remoteUser) {
    this.remoteUser = remoteUser;
  }

  /**
   * Gets subscriberGuid.
   *
   * @return Value of subscriberGuid.
   */
  public String getSubscriberGuid() {
    return subscriberGuid;
  }

  /**
   * Sets new subscriberGuid.
   *
   * @param subscriberGuid New value of subscriberGuid.
   */
  public void setSubscriberGuid(String subscriberGuid) {
    this.subscriberGuid = subscriberGuid;
  }

  /**
   * Gets dataConsent.
   *
   * @return Value of dataConsent.
   */
  public DataConsent getDataConsent() {
    return dataConsent;
  }

  /**
   * Sets new dataConsent.
   *
   * @param dataConsent New value of dataConsent.
   */
  public void setDataConsent(DataConsent dataConsent) {
    this.dataConsent = dataConsent;
  }

  /**
   * Gets waiver.
   *
   * @return Value of waiver.
   */
  public String getWaiver() {
    return waiver;
  }

  /**
   * Sets new waiver.
   *
   * @param waiver New value of waiver.
   */
  public void setWaiver(String waiver) {
    this.waiver = waiver;
  }

  /**
   * Gets emergencyContactName.
   *
   * @return Value of emergencyContactName.
   */
  public String getEmergencyContactName() {
    return emergencyContactName;
  }

  /**
   * Sets new emergencyContactName.
   *
   * @param emergencyContactName New value of emergencyContactName.
   */
  public void setEmergencyContactName(String emergencyContactName) {
    this.emergencyContactName = emergencyContactName;
  }

  /**
   * Gets updateDate.
   *
   * @return Value of updateDate.
   */
  public String getUpdateDate() {
    return updateDate;
  }

  /**
   * Sets new updateDate.
   *
   * @param updateDate New value of updateDate.
   */
  public void setUpdateDate(String updateDate) {
    this.updateDate = updateDate;
  }

  @Override
  public String toString() {
    return
        "Subscription{" +
            "generation = '" + generation + '\'' +
            ",remoteUser = '" + remoteUser + '\'' +
            ",subscriptions = '" + subscriptions + '\'' +
            ",updateDate = '" + updateDate + '\'' +
            ",emergencyContactName = '" + emergencyContactName + '\'' +
            ",emergencyContactNumber = '" + emergencyContactNumber + '\'' +
            ",subscriberGuid = '" + subscriberGuid + '\'' +
            ",createSource = '" + createSource + '\'' +
            ",updateSource = '" + updateSource + '\'' +
            ",dofu = '" + dofu + '\'' +
            ",termsAccecptanceDate = '" + termsAccecptanceDate + '\'' +
            ",contractID = '" + contractID + '\'' +
            ",vin = '" + vin + '\'' +
            ",region = '" + region + '\'' +
            ",waiver = '" + waiver + '\'' +
            ",dataConsent = '" + dataConsent + '\'' +
            ",createDate = '" + createDate + '\'' +
            "}";
  }
}