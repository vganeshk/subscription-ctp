package com.tmna.ct.telematics.subscription.outbound;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.http.HttpStatus;

import java.util.List;

/**
 * This class is used to hold the POJO for setting the message in service response
 *
 * @author Srini Y
 * @version 1.0
 */
@JsonIgnoreProperties(value = {"httpCode"})
public class Status {


  private HttpStatus httpCode;

  private List<Message> messages;

  /**
   * Get the list of messages
   *
   * @return messages
   */
  public List<Message> getMessages() {
    return messages;
  }

  /**
   * Set the list of message
   *
   * @param messages messages
   */
  public void setMessages(List<Message> messages) {
    this.messages = messages;
  }

  /**
   * Gets httpCode.
   *
   * @return Value of httpCode.
   */
  public HttpStatus getHttpCode() {
    return httpCode;
  }

  /**
   * Sets new httpCode.
   *
   * @param httpCode New value of httpCode.
   */
  public void setHttpCode(HttpStatus httpCode) {
    this.httpCode = httpCode;
  }
}
