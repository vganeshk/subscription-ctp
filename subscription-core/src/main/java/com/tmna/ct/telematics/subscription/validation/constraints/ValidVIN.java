package com.tmna.ct.telematics.subscription.validation.constraints;

import com.tmna.ct.telematics.subscription.validation.VINValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 */
@Target({ ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = VINValidator.class)
public @interface ValidVIN {

    String message() default "{CODE.INVALID.INCORRECT.SUBSCRIPTION.ATTRIBUTE}:{MESSAGE.INVALID.SUBSCRIPTION.VIN}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
