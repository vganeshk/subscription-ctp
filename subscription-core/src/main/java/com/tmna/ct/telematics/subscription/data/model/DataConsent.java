package com.tmna.ct.telematics.subscription.data.model;

public class DataConsent {
  private String serviceConnect;
  private String can300;
  private String dealerContact;
  private String ubi;

  /**
   * Gets dealerContact.
   *
   * @return Value of dealerContact.
   */
  public String getDealerContact() {
    return dealerContact;
  }

  /**
   * Sets new dealerContact.
   *
   * @param dealerContact New value of dealerContact.
   */
  public void setDealerContact(String dealerContact) {
    this.dealerContact = dealerContact;
  }

  /**
   * Gets ubi.
   *
   * @return Value of ubi.
   */
  public String getUbi() {
    return ubi;
  }

  /**
   * Sets new ubi.
   *
   * @param ubi New value of ubi.
   */
  public void setUbi(String ubi) {
    this.ubi = ubi;
  }

  /**
   * Gets serviceConnect.
   *
   * @return Value of serviceConnect.
   */
  public String getServiceConnect() {
    return serviceConnect;
  }

  /**
   * Sets new serviceConnect.
   *
   * @param serviceConnect New value of serviceConnect.
   */
  public void setServiceConnect(String serviceConnect) {
    this.serviceConnect = serviceConnect;
  }

  /**
   * Gets can300.
   *
   * @return Value of can300.
   */
  public String getCan300() {
    return can300;
  }

  /**
   * Sets new can300.
   *
   * @param can300 New value of can300.
   */
  public void setCan300(String can300) {
    this.can300 = can300;
  }

  @Override
  public String toString() {
    return
        "DataConsent{" +
            "serviceConnect = '" + serviceConnect + '\'' +
            ",can300 = '" + can300 + '\'' +
            ",dealerContact = '" + dealerContact + '\'' +
            ",ubi = '" + ubi + '\'' +
            "}";
  }

}
