package com.tmna.ct.telematics.subscription.validation;

import com.tmna.ct.telematics.subscription.utils.ValidationRegexUtils;
import com.tmna.ct.telematics.subscription.validation.constraints.ValidVIN;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Pattern;

/**
 * This class is a custom validator to validate VIN.
 * @author Srini Y
 * @version 1.0
 */

public class VINValidator implements ConstraintValidator<ValidVIN, String> {

    @Override
    public boolean isValid(String vin, ConstraintValidatorContext constraintValidatorContext) {
        return vin != null && Pattern.matches(ValidationRegexUtils.VIN_REGEX,vin);
    }

    @Override
    public void initialize(ValidVIN validVIN) {
        //Not used. Method Overridden
    }
}
