package com.tmna.ct.telematics.subscription.utils;

/**
 * This class contains constant variables used for Subscription.
 *
 * @author Srini Y
 * @version 1.0
 */
public class Constants {

  /**
   * Adding a explicit constructor to avoid initialization.
   * Throws a illegal state exception when a object is created.
   */
  private Constants() {
    throw new IllegalStateException("Constants");
  }

  // Constants for headers.
  public static final String X_BRAND = "X-BRAND";
  public static final String X_CHANNEL = "X-CHANNEL";
  public static final String X_CORRELATIONID = "X-CORRELATIONID";
  public static final String CONTENT_TYPE = "CONTENT-TYPE";
  public static final String DATETIME = "DATETIME";
  public static final String AUTHORIZATION = "Authorization";
  public static final String ACCEPT_ENCODING = "Accept-Encoding";

  //Constants for headers Message
  public static final String INCORRECT_INVALID_CONTENT_TYPE = "Incorrect/Invalid value for Content Type in Header. Valid value is application/json.";
  public static final String INCORRECT_INVALID_X_BRAND = "Incorrect/Invalid value for X_Brand in Header. Valid value are either T or L.";
  public static final String INCORRECT_INVALID_X_CHANNEL = "Incorrect/Invalid value for X-Channel in Header. Valid value are either TC, TSC, LER, TRC, TARP_SELF, TARP_DEALER, TARP_AGENT or ADMIN_TOOL.";
  public static final String INCORRECT_INVALID_X_CORRELATION_ID = "Incorrect/Invalid value for X-Correlation ID in Header.";
  public static final String INCORRECT_INVALID_AUTHORIZATION = "Incorrect/Invalid value for Authorization in Header.";
  public static final String INCORRECT_INVALID_ACCEPT_ENCODING = "Incorrect/Invalid value for Accept-Encoding in Header.";
  public static final String INCORRECT_INVALID_DATETIME = "Incorrect/Invalid value for Datetime in Header.";
  public static final String MISSING_HEADER_CONTENT_TYPE = " Missing Content Type value in Header.";
  public static final String MISSING_HEADER_X_BRAND = " Missing X-Brand value in Header.";
  public static final String MISSING_HEADER_X_CHANNEL = " Missing X-Channel value in Header.";
  public static final String MISSING_HEADER_X_CORRELATION_ID = " Missing X-Correlation Id value in Header.";
  public static final String MISSING_HEADER_AUTHORIZATION = " Missing Authorization value in Header.";
  public static final String MISSING_HEADER_DATETIME = " Missing Datetime value in Header.";
  public static final String MISSING_HEADER_ACCEPT_HEADER = " Missing Accept-Encoding value in Header.";

  // Constants for Response codes.
  public static final String SUBSCRIPTION_CREATION_CODE = "SUB-0000";
  public static final String MANDATORY_HEADER_MISSING_CODE = "SUB-0001";
  public static final String INCORRECT_HEADER_CODE = "SUB-0002";
  public static final String INTERNAL_SERVER_ERROR_CODE = "SUB-0006";
  public static final String VIN_NOT_FOUND_CODE = "SUB-0008";
  public static final String CORRELATION_ID_NOT_FOUND_CODE = "SUB-0009";

  // Constants for Response message
  public static final String MANDATORY_HEADER_MISSING = "Mandatory Header Attributes Missing";
  public static final String INCORRECT_HEADER_VALUE = "Incorrect/Invalid Header Values";
  public static final String INTERNAL_SERVER_ERROR = "Internal Server Error Occurred";
  public static final String REQUEST_EXECUTED_SUCCESSFULLY = "Request Executed Successfully";
  public static final String SUBSCRIPTION_CREATED_SUCCESSFULLY = "Subscription created Successfully";
  public static final String VIN_NOT_FOUND = "VIN not found";
  public static final String CORRELATIOND_ID_NOT_FOUND = "CorrelationID not found";
  public static final String INCORRECT_INVALID_BODY_ATTRIBUTES = "Incorrect/Invalid Request Body Attribute(s)";

  // Constants for logger
  public static final String METHOD_ENTRY = "Entry";
  public static final String METHOD_EXIT_SUCCESSFUL = "Exited Successfully";

  // Generic Constants
  public static final String VIN = "VIN";
  public static final String CORRELATION_ID="CORRELATIOND_ID";

  public static final String DHC_STATUS = "ON";
}
