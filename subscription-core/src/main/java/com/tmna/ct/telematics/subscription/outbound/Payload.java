package com.tmna.ct.telematics.subscription.outbound;

import com.tmna.ct.telematics.subscription.data.model.Subscription;

import java.util.List;

/**
 * This class is used to set the list of customer in response payload.
 *
 * @author Srini Y
 * @version 1.0
 */
public class Payload {

  private Subscription subscription;

  private List<Subscription> subscriptionHistory;

  private int page;

  private Long count;

  /**
   * Gets subscription.
   *
   * @return Value of subscription.
   */
  public Subscription getSubscription() {
    return subscription;
  }

  /**
   * Sets new subscription.
   *
   * @param subscription New value of subscription.
   */
  public void setSubscription(Subscription subscription) {
    this.subscription = subscription;
  }

  /**
   * Sets new subscriptionHistory.
   *
   * @param subscriptionHistory New value of subscriptionHistory.
   */
  public void setSubscriptionHistory(List<Subscription> subscriptionHistory) {
    this.subscriptionHistory = subscriptionHistory;
  }

  /**
   * Gets subscriptionHistory.
   *
   * @return Value of subscriptionHistory.
   */
  public List<Subscription> getSubscriptionHistory() {
    return subscriptionHistory;
  }

  /**
   * Sets new page.
   *
   * @param page New value of page.
   */
  public void setPage(int page) {
    this.page = page;
  }

  /**
   * Gets count.
   *
   * @return Value of count.
   */
  public Long getCount() {
    return count;
  }

  /**
   * Gets page.
   *
   * @return Value of page.
   */
  public int getPage() {
    return page;
  }

  /**
   * Sets new count.
   *
   * @param count New value of count.
   */
  public void setCount(Long count) {
    this.count = count;
  }
}
