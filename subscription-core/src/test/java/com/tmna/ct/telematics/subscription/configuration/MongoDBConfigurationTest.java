package com.tmna.ct.telematics.subscription.configuration;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

/**
 * MongoDBConfiguration test class.
 *
 * @author Srini Y
 * @version 1.0
 */
@SpringBootTest(classes = MongoDBConfiguration.class)
@RunWith(SpringRunner.class)
public class MongoDBConfigurationTest {

  @Autowired
  private MongoDBConfiguration mongoConfiguration;

  /**
   * This method is used to assert the collection name.
   */
  @Test
  public void shouldFetchCustomerCollectionName() {
    assertEquals("subscription", mongoConfiguration.subscriptionCollectionName());
  }
}