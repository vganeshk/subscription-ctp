package com.tmna.ct.telematics.subscription.utils;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmna.ct.telematics.subscription.data.model.Subscription;
import com.tmna.ct.telematics.subscription.outbound.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
public class ResponseUtilsTest {

  @InjectMocks
  private ResponseUtils responseUtils;

  private Subscription subscription;

  private Validator validator;

  @Before
  public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    String subscriptionDetailsValue = new String(
        Files.readAllBytes(Paths
            .get(ResponseUtilsTest.class.getClassLoader().getResource("SubscriptionRequest.json").toURI())),
        Charset.forName("utf-8"));
    ObjectMapper objectMapper = new ObjectMapper();
    objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    subscription = objectMapper.readValue(subscriptionDetailsValue, Subscription.class);
    ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    validator = factory.getValidator();

    HttpHeaders httpHeaders;
    httpHeaders = new HttpHeaders();
    httpHeaders.add("CONTENT-TYPE","application/json");
    httpHeaders.add("X-BRAND","L");
    httpHeaders.add("X-CHANNEL","TARP_SELF");
    httpHeaders.add("X-CORRELATIONID","997a3649-523f-403b-82b8-4c2f53e0iu89");
    httpHeaders.add("Authorization","icZBMDlw2XOtuTLIZkfeFi1vTE8QXr4lr8tA6b7t");
    httpHeaders.add("DATETIME","1520911254125");

  }

  /**
   * this method is used to test the success subscription creation
   */
  @Test
  public void buildSubscriptionResponseTest() {
    SubscriptionResponse subscriptionResponse = responseUtils.buildSubscriptionResponse(subscription);
    assertEquals(HttpStatus.OK, subscriptionResponse.getStatus().getHttpCode());
  }

  /**
   * this method is used to test the success get subscription
   */
  @Test
  public void buildSearchSubscriptionResponseTest() {
    GetStatusResponse getStatusResponse = responseUtils.buildGetStatusSuccessResponse(subscription);
    assertEquals(HttpStatus.OK, getStatusResponse.getStatus().getHttpCode());
    assertNotNull(getStatusResponse.getPayload());
    assertNotNull(getStatusResponse.getPayload().getSubscription());
  }


/**
   * this method is used to test the VIN Not Found exception.
   */

  public void buildGetStatusNotFoundResponseTest() {
    GetStatusResponse getStatusResponse = responseUtils.buildGetStatusNotFoundResponse("VIN");
    assertEquals("200", getStatusResponse.getStatus().getHttpCode());
  }


  /**
   * this method is used to test the exception scenarios
   */
  @Test
  public void buildCustomerResponseException(){
    Status status = responseUtils.buildSubscriptionErrorResponse();
    ErrorResponse errorResponse = new ErrorResponse(status);
    assertNotNull(errorResponse.getStatus());
    assertEquals("SUB-0006", status.getMessages().get(0).getResponseCode());
    assertEquals("Internal Server Error Occurred",status.getMessages().get(0).getDescription());
    assertEquals("Internal Server Error Occurred",status.getMessages().get(0).getDetailedDescription());
  }

  /**
   * this method is used to test the Business error scenarios.
   */
  @Test
  public void buildSubscriptionResponseValidationTest(){
    //Test to validate the above method
  }

  /**
   * this method is used to test the header validation response
   */
  @Test
  public void buildGetStatusResponseTest(){
    List<Message> messageList = new ArrayList<>();
    Message message = new Message();
    message.setResponseCode(Constants.INCORRECT_HEADER_CODE);
    message.setDescription(Constants.INCORRECT_HEADER_VALUE);
    message.setDetailedDescription(Constants.INCORRECT_HEADER_VALUE);
    messageList.add(message);

    GetStatusResponse getStatusResponse = responseUtils.buildGetStatusResponse(messageList);
    assertEquals(HttpStatus.BAD_REQUEST, getStatusResponse.getStatus().getHttpCode());
  }

  @Test
  public void buildGetStatusVINNotFoundResponseTest(){
    GetStatusResponse getStatusResponse = responseUtils.buildGetStatusNotFoundResponse("VIN");
    assertEquals(HttpStatus.NOT_FOUND, getStatusResponse.getStatus().getHttpCode());
  }

  @Test
  public void buildGetStatusCIDNotFoundResponseTest(){
    GetStatusResponse getStatusResponse = responseUtils.buildGetStatusNotFoundResponse("CORRELATIOND_ID");
    assertEquals(HttpStatus.NOT_FOUND, getStatusResponse.getStatus().getHttpCode());
  }

  @Test
  public void buildSaveProvCIDNotFoundResponseTest(){
    SubscriptionResponse subscriptionResponse = responseUtils.buildCIDNotFoundResponse("CORRELATIOND_ID");
    assertEquals(HttpStatus.NOT_FOUND, subscriptionResponse.getStatus().getHttpCode());
  }

  @Test
  public void buildSubscriptionErrorResponseTest(){
    List<Message> messageList = new ArrayList<>();
    Message message = new Message();
    message.setResponseCode(Constants.INCORRECT_HEADER_CODE);
    message.setDescription(Constants.INCORRECT_HEADER_VALUE);
    message.setDetailedDescription(Constants.INCORRECT_HEADER_VALUE);
    messageList.add(message);

    SubscriptionResponse subscriptionResponse = responseUtils.buildSubscriptionErrorResponse(messageList);
    assertEquals(HttpStatus.BAD_REQUEST, subscriptionResponse.getStatus().getHttpCode() );
  }

  @Test
  public void buildMessageSuccessTest(){
    Message message = responseUtils.buildMessage(Constants.INCORRECT_HEADER_CODE, Constants.INCORRECT_HEADER_VALUE, Constants.INCORRECT_HEADER_VALUE);
    assertNotNull(message);
    assertEquals("SUB-0002",message.getResponseCode());
  }
}