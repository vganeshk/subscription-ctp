## INTRODUCTION

Subscription will contain a family of micro services to facilitate all the CRUD operations related to customer's subscription information.

## PREREQUISITES
* `Java 1.8` and `Maven` are needed to build/run this project
* `docker` and `docker-compose` are needed to run the services together
* an IDE or editor that supports [EditorConfig](http://editorconfig.org/) is recommended


## BUILD STATUS

## PROJECT CODE STRUCTURE

#### subscription-core
This module contains code that will be shared across all other modules with in this family. Common models, services and http filters would be examples of such shared code.

#### subscription-management
Service Definition



```
.
│
├── docker-compose.yml
├── pom.xml
│
├── subscription-core
│   ├── pom.xml
│   └── src
│       ├── main
│       │   ├── java/com/tmna/ct/telematics/subscription/core/*.java
│       │ 
│       └── test
│			├── java/com/tmna/ct/telematics/subscription/core/*.java
│
├── subscription-service
│   ├── Dockerfile
│   ├── pom.xml
│   └── src
│       ├── main
│       │   ├── java/com/tmna/ct/telematics/subscription/subscription-service/Application.java
│       │   └── resources
│       │       └── application.yml
│       └── test/java/com/tmna/ct/telematics/subscription/subscription-service/ApplicationTest.java
│
```

## GETTING STARTED
1. make sure `docker` is enabled
2. open a terminal/console window
3. `cd path/to/subscription`
4. `mvn clean package` to build the services
5. `docker-compose build` to build docker images
5. `docker-compose up` to run the services together


## INTEGRATION TESTING
