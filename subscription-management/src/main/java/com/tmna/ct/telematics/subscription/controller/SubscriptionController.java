package com.tmna.ct.telematics.subscription.controller;

import com.tc.logging.ILogService;
import com.tc.logging.LogFactory;
import com.tc.logging.annotation.LoggingMetadata;
import com.tmna.ct.telematics.subscription.data.model.Subscription;
import com.tmna.ct.telematics.subscription.outbound.GetStatusResponse;
import com.tmna.ct.telematics.subscription.outbound.SubscriptionResponse;
import com.tmna.ct.telematics.subscription.service.ISubscriptionService;
import com.tmna.ct.telematics.subscription.utils.Constants;
import com.tmna.ct.telematics.subscription.utils.LogUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * SubscriptionController is used to define the controller for the Get & save subscription services.
 *
 * @author Srini Y
 * @version 1.0
 *
 **/
@LoggingMetadata(feature = "SubscriptionController")
@RestController
public class SubscriptionController {

  private static final ILogService logger = LogFactory.getLogger(SubscriptionController.class);

  @Autowired
  private ISubscriptionService subscriptionService;

  /**
   * This method is used to invoke the validations & subscription service to create a subscription.
   * @param requestBody Incoming request body
   * @param httpHeaders Incoming request headers
   * @return responseEntity
   */
  @LoggingMetadata(goal = "createSubscription")
  @RequestMapping(value = "/v1/create", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<SubscriptionResponse> createSubscription(@RequestBody Subscription subscription, @RequestHeader HttpHeaders httpHeaders) {
    LogUtils.logMethodEntry(logger);
    SubscriptionResponse subscriptionResponse = subscriptionService.createSubscription(subscription, httpHeaders);
    ResponseEntity<SubscriptionResponse> responseResponseEntity = new ResponseEntity<>(subscriptionResponse, subscriptionResponse.getStatus().getHttpCode());
    LogUtils.logMethodExit(logger, Constants.METHOD_EXIT_SUCCESSFUL);
    return responseResponseEntity;
  }

  /**
   * This method is used to invoke the validations & subscription service to fetch a subscription.
   * @param httpHeaders Incoming request headers
   * @return responseEntity
   */
  @LoggingMetadata(goal = "getStatus")
  @RequestMapping(value = "/v1/status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<GetStatusResponse> getStatus(@RequestHeader HttpHeaders httpHeaders) {
    LogUtils.logMethodEntry(logger);
    GetStatusResponse getStatusResponse = subscriptionService.getStatusByVin(httpHeaders);
    ResponseEntity<GetStatusResponse> responseResponseEntity;
    if (HttpStatus.BAD_REQUEST.equals(getStatusResponse.getStatus().getHttpCode())) {
      responseResponseEntity = new ResponseEntity<>(getStatusResponse, HttpStatus.BAD_REQUEST);
    } else {
      responseResponseEntity = new ResponseEntity<>(getStatusResponse, getStatusResponse.getStatus().getHttpCode());
    }
    LogUtils.logMethodExit(logger, Constants.METHOD_EXIT_SUCCESSFUL);
    return responseResponseEntity;
  }

  /**
   * This method is used to invoke the validations & subscription service to create a subscription.
   * @param httpHeaders Incoming request headers
   * @return responseEntity
   */
  @LoggingMetadata(goal = "checkForVinExists")
  @RequestMapping(value = "/v1/vinCheck", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<String> vinCheck(@RequestHeader HttpHeaders httpHeaders) {
    LogUtils.logMethodEntry(logger);
    SubscriptionResponse subscriptionResponse = subscriptionService.checkVinExist(httpHeaders);
    String vinFound="{\"found\":\"true\"}";
    String vinNotFound="{\"found\":\"false\"}";
    ResponseEntity<String> responseResponseEntity;
    if (HttpStatus.OK.equals(subscriptionResponse.getStatus().getHttpCode())) {
      responseResponseEntity = new ResponseEntity<>(vinFound, HttpStatus.OK);
    } else {
      responseResponseEntity = new ResponseEntity<>(vinNotFound, HttpStatus.OK);
    }
    LogUtils.logMethodExit(logger, Constants.METHOD_EXIT_SUCCESSFUL);
    return responseResponseEntity;
  }

  /**
   * This method is used to invoke the validations & subscription service to create a subscription.
   * @param requestBody Incoming request body
   * @param httpHeaders Incoming request headers
   * @return responseEntity
   */
  @LoggingMetadata(goal = "updateSubscription")
  @RequestMapping(value = "/v1/update", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<SubscriptionResponse> updateSubscription(@RequestBody String requestBody, @RequestHeader HttpHeaders httpHeaders) {
    LogUtils.logMethodEntry(logger);
    SubscriptionResponse subscriptionResponse = subscriptionService.updateSubscription(requestBody, httpHeaders);
    ResponseEntity<SubscriptionResponse> responseResponseEntity = new ResponseEntity<>(subscriptionResponse, subscriptionResponse.getStatus().getHttpCode());
    LogUtils.logMethodExit(logger, Constants.METHOD_EXIT_SUCCESSFUL);
    return responseResponseEntity;
  }

  /**
   * This method is used to invoke the validations & subscription service to fetch a subscription.
   * @param httpHeaders Incoming request headers
   * @return responseEntity
   */
  @LoggingMetadata(goal = "getSubscriptionHistory")
  @RequestMapping(value = "/v1/history", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<GetStatusResponse> getSubscriptionHistory(@RequestHeader HttpHeaders httpHeaders) {
    LogUtils.logMethodEntry(logger);
    GetStatusResponse getStatusResponse = subscriptionService.getSubscriptionHistory(httpHeaders);
    ResponseEntity<GetStatusResponse> responseResponseEntity;
    if (HttpStatus.BAD_REQUEST.equals(getStatusResponse.getStatus().getHttpCode())) {
      responseResponseEntity = new ResponseEntity<>(getStatusResponse, HttpStatus.BAD_REQUEST);
    } else {
      responseResponseEntity = new ResponseEntity<>(getStatusResponse, getStatusResponse.getStatus().getHttpCode());
    }
    LogUtils.logMethodExit(logger, Constants.METHOD_EXIT_SUCCESSFUL);
    return responseResponseEntity;
  }
}
