package com.tmna.ct.telematics.subscription.service;

import com.tmna.ct.telematics.subscription.data.model.Subscription;
import com.tmna.ct.telematics.subscription.outbound.Message;
import java.util.List;
import java.util.Set;

import org.springframework.http.HttpHeaders;

import javax.validation.ConstraintViolation;

public interface IValidationService {

  List<Message> validateRequestHeaders(HttpHeaders requestHeader, List<String> doNotValidateHeaderList);

  Set<ConstraintViolation<Subscription>> validateSubscription(Subscription subscription);

}
