package com.tmna.ct.telematics.subscription.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tc.logging.ILogService;
import com.tc.logging.LogFactory;
import com.tc.logging.annotation.LoggingMetadata;
import com.tmna.ct.telematics.subscription.data.dao.IHistorySubscriptionDao;
import com.tmna.ct.telematics.subscription.data.dao.ISubscriptionDao;
import com.tmna.ct.telematics.subscription.data.model.HistorySubscription;
import com.tmna.ct.telematics.subscription.data.model.Subscription;
import com.tmna.ct.telematics.subscription.outbound.GetStatusResponse;
import com.tmna.ct.telematics.subscription.outbound.Message;
import com.tmna.ct.telematics.subscription.outbound.Status;
import com.tmna.ct.telematics.subscription.outbound.SubscriptionResponse;
import com.tmna.ct.telematics.subscription.service.ISubscriptionService;
import com.tmna.ct.telematics.subscription.service.IValidationService;
import com.tmna.ct.telematics.subscription.utils.Constants;
import com.tmna.ct.telematics.subscription.utils.LogUtils;
import com.tmna.ct.telematics.subscription.utils.ResponseUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolation;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * This class implements the subscription service's method for CRUD operations on OCPR.
 *
 * @author Srini Y
 * @version 1.0
 */
@LoggingMetadata(feature = "SubscriptionServiceImpl")
@Service
public class SubscriptionServiceImpl implements ISubscriptionService {

  private static final ILogService logger = LogFactory.getLogger(SubscriptionServiceImpl.class);

  @Autowired
  IValidationService validationService;

  @Autowired
  ISubscriptionDao subscriptionDao;

  @Autowired
  IHistorySubscriptionDao historySubscriptionDao;

  /**
   * This method is used to invoke DAO to fetch a subscription.
   *
   * @param httpHeaders Incoming Http headers
   * @return GetStatusResponse
   */
  @LoggingMetadata(goal = "getStatusByVin")
  @Override
  public GetStatusResponse getStatusByVin(HttpHeaders httpHeaders) {
    LogUtils.logMethodEntry(logger);
    Subscription dbSubscription = null;
    ResponseUtils responseUtils = new ResponseUtils();
    GetStatusResponse getStatusResponse = null;
    List<Message> headerValidationList = validationService.validateRequestHeaders(httpHeaders, null);
    if (headerValidationList.isEmpty()) {
      if (!httpHeaders.getFirst("Vin").isEmpty()) {
        dbSubscription = subscriptionDao.findByVin(httpHeaders.getFirst("Vin"));
      }
      if (dbSubscription != null) {
        getStatusResponse = responseUtils.buildGetStatusSuccessResponse(dbSubscription);
      } else {
        getStatusResponse = responseUtils.buildGetStatusNotFoundResponse(Constants.VIN);
      }
    } else {
      getStatusResponse = responseUtils.buildGetStatusResponse(headerValidationList);
    }
    LogUtils.logMethodExit(logger, Constants.METHOD_EXIT_SUCCESSFUL);
    return getStatusResponse;
  }

  /**
   * This method is used to invoke the DAO t save the incoming provision.
   *
   * @param requestBody Incoming Request Body
   * @param httpHeaders Incoming Request Headers
   * @return SubscriptionResponse
   */
  @LoggingMetadata(goal = "createSubscription")
  @Override
  public SubscriptionResponse createSubscription(Subscription subscription, HttpHeaders httpHeaders) {
    LogUtils.logMethodEntry(logger);
    //ObjectMapper objectMapper = new ObjectMapper();
    SubscriptionResponse subscriptionResponse = null;
    //Subscription subscription;
    ResponseUtils responseUtils = new ResponseUtils();
    //try {
      //subscription = objectMapper.readValue(requestBody, Subscription.class);
      List<Message> headerValidationList = validationService.validateRequestHeaders(httpHeaders, null);
      Set<ConstraintViolation<Subscription>> subscriptionSet = validationService.validateSubscription(subscription);
      if (headerValidationList.isEmpty() && subscriptionSet.isEmpty()) {
        Subscription dbSubscription = subscriptionDao.findByVin(subscription.getVin());
        HistorySubscription historySubscription = new HistorySubscription();
        historySubscription.setSubscription(subscription);
        historySubscription.setVin(subscription.getVin());
        historySubscriptionDao.save(historySubscription);
        if (dbSubscription != null) {
          Subscription updatedSubscription = this.resolveSubscriptionRequest(dbSubscription, subscription);
          subscriptionDao.save(updatedSubscription);
        } else {
          subscriptionDao.save(subscription);
        }
        subscriptionResponse = responseUtils.buildSubscriptionResponse(subscription);
      } else {
        subscriptionResponse = responseUtils.buildSubscriptionResponse(subscriptionSet, headerValidationList);
      }
    /*} catch (IOException e) {
      logger.error(e.getMessage());
    }*/
    LogUtils.logMethodExit(logger, Constants.METHOD_EXIT_SUCCESSFUL);
    return subscriptionResponse;
  }

  /**
   * This method is used to check if a VIN exist in DB
   *
   * @param httpHeaders is the list of Headers
   * @return SubscriptionResponse which has the status
   */
  @LoggingMetadata(goal = "checkVinExist")
  @Override
  public SubscriptionResponse checkVinExist(HttpHeaders httpHeaders) {
    LogUtils.logMethodEntry(logger);
    ResponseUtils responseUtils = new ResponseUtils();
    SubscriptionResponse subscriptionResponse = new SubscriptionResponse();
    subscriptionResponse.setStatus(new Status());
    List<Message> headerValidationList = validationService.validateRequestHeaders(httpHeaders, null);
    if (headerValidationList.isEmpty()) {
      Boolean vinFlag = subscriptionDao.existsByVin(httpHeaders.getFirst("Vin"));
      if (vinFlag) {
        subscriptionResponse.getStatus().setHttpCode(HttpStatus.OK);
      } else {
        subscriptionResponse.getStatus().setHttpCode(HttpStatus.NOT_FOUND);
      }
    } else {
      subscriptionResponse = responseUtils.buildSubscriptionErrorResponse(headerValidationList);
    }
    return subscriptionResponse;
  }

  /**
   * This method is used to invoke the DAO t save the incoming provision.
   *
   * @param requestBody Incoming Request Body
   * @param httpHeaders Incoming Request Headers
   * @return SubscriptionResponse
   */
  @LoggingMetadata(goal = "updateSubscription")
  @Override
  public SubscriptionResponse updateSubscription(String requestBody, HttpHeaders httpHeaders) {
    LogUtils.logMethodEntry(logger);
    ObjectMapper objectMapper = new ObjectMapper();
    SubscriptionResponse subscriptionResponse = null;
    Subscription subscription;
    ResponseUtils responseUtils = new ResponseUtils();
    try {
      subscription = objectMapper.readValue(requestBody, Subscription.class);
      List<String> doNotValidateHeaderList = new ArrayList<>();
      doNotValidateHeaderList.add(Constants.X_BRAND);
      List<Message> headerValidationList = validationService.validateRequestHeaders(httpHeaders, doNotValidateHeaderList);
      Set<ConstraintViolation<Subscription>> subscriptionSet = validationService.validateSubscription(subscription);
      if (headerValidationList.isEmpty() && subscriptionSet.isEmpty()) {
        Subscription dbSubscription = subscriptionDao.findByVin(subscription.getVin());
        HistorySubscription historySubscription = new HistorySubscription();
        historySubscription.setSubscription(subscription);
        historySubscription.setVin(subscription.getVin());
        historySubscriptionDao.save(historySubscription);
        if (dbSubscription != null) {
          Subscription updatedSubscription = this.resolveSubscription(dbSubscription, subscription);
          subscriptionDao.save(updatedSubscription);
          subscriptionResponse = responseUtils.buildSubscriptionResponse(subscription);
        } else {
          subscriptionResponse = responseUtils.buildCIDNotFoundResponse(Constants.CORRELATION_ID);
        }
      } else {
        subscriptionResponse = responseUtils.buildSubscriptionResponse(subscriptionSet, headerValidationList);
      }
    } catch (IOException e) {
      logger.error(e.getMessage());
    }
    LogUtils.logMethodExit(logger, Constants.METHOD_EXIT_SUCCESSFUL);
    return subscriptionResponse;
  }

  @Override
  public GetStatusResponse getSubscriptionHistory(HttpHeaders httpHeaders) {
    LogUtils.logMethodEntry(logger);
    ResponseUtils responseUtils = new ResponseUtils();
    GetStatusResponse getStatusResponse = null;
    Page<HistorySubscription> page = null;
    List<Message> headerValidationList = validationService.validateRequestHeaders(httpHeaders, null);
    if (headerValidationList.isEmpty()) {
      if (!httpHeaders.getFirst("Vin").isEmpty()) {
        org.springframework.data.domain.Pageable pageable = new PageRequest(Integer.parseInt(httpHeaders.getFirst("page")),Integer.parseInt(httpHeaders.getFirst("size")));
        page = historySubscriptionDao.findAllByVin(httpHeaders.getFirst("Vin"), pageable);
      }
      if (page != null && page.getContent() != null && !page.getContent().isEmpty()) {
        getStatusResponse = responseUtils.buildHistorySubscriptionSuccessResponse(page.getContent(), page.getTotalElements(), page.getTotalPages());
      } else {
        getStatusResponse = responseUtils.buildGetStatusNotFoundResponse(Constants.VIN);
      }
    } else {
      getStatusResponse = responseUtils.buildGetStatusResponse(headerValidationList);
    }
    LogUtils.logMethodExit(logger, Constants.METHOD_EXIT_SUCCESSFUL);
    return getStatusResponse;
  }

  /**
   * This method is used to update the incoming SPEC I object.
   *
   * @param existingSubscription Existing subscription from DB
   * @param subscription         Incoming subscription object
   * @return resolved subscription object
   */
  @LoggingMetadata(goal = "resolveSubscription")
  private Subscription resolveSubscription(Subscription existingSubscription, Subscription subscription) {
    LogUtils.logMethodEntry(logger);
   // Perform operation update existing subscription with incoming updates. This can be eliminated based on need.
    LogUtils.logMethodExit(logger, Constants.METHOD_EXIT_SUCCESSFUL);
    return existingSubscription;
  }

  /**
   * This method is used to resolve the difference between existing & incoming subscription object.
   *
   * @param existingSubscription Existing subscription in DB
   * @param incomingSubscription Incoming subscription
   * @return resolved subscription object
   */
  @LoggingMetadata(goal = "resolveSubscriptionRequest")
  private Subscription resolveSubscriptionRequest(Subscription existingSubscription, Subscription incomingSubscription) {
    LogUtils.logMethodEntry(logger);
    // Perform operation to compare the incoming & existing object in case of update.
    LogUtils.logMethodExit(logger, Constants.METHOD_EXIT_SUCCESSFUL);
    return existingSubscription;
  }
}


