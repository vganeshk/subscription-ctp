package com.tmna.ct.telematics.subscription.service.impl;

import com.tc.logging.ILogService;
import com.tc.logging.LogFactory;
import com.tc.logging.annotation.LoggingMetadata;
import com.tmna.ct.telematics.subscription.data.model.Subscription;
import com.tmna.ct.telematics.subscription.outbound.Message;
import com.tmna.ct.telematics.subscription.service.IValidationService;
import com.tmna.ct.telematics.subscription.utils.Constants;
import com.tmna.ct.telematics.subscription.utils.LogUtils;
import com.tmna.ct.telematics.subscription.utils.ResponseUtils;
import com.tmna.ct.telematics.subscription.utils.ValidationRegexUtils;
import com.tmna.ct.telematics.subscription.validation.group.*;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

import javax.validation.*;
import java.util.*;
import java.util.regex.Pattern;

/**
 * This method is used to define the validations performed on subscription.
 *
 * @author Srini Y
 * @version 1.0
 */
@LoggingMetadata(feature = "ValidationServiceImpl")
@Service
public class ValidationServiceImpl implements IValidationService {

  private static final ILogService logger = LogFactory.getLogger(ValidationServiceImpl.class);

  /**
   * this method is used to validate the request headers
   *
   * @param requestHeader incoming Request headers
   * @return list of messages
   */
  public List<Message> validateRequestHeaders(HttpHeaders requestHeader, List<String> doNotValidateHeaderList) {
    LogUtils.logMethodEntry(logger);
    List<Message> messageList = new ArrayList<>();
    //Validating the content type
    if ( !(doNotValidateHeaderList != null && doNotValidateHeaderList.contains(Constants.CONTENT_TYPE))) {
      messageList.addAll(this.validateContentTypeHeader(requestHeader));
    }

    //Validating the brand
    if (!(doNotValidateHeaderList != null && doNotValidateHeaderList.contains(Constants.X_BRAND))) {
      messageList.addAll(this.validateXBrandHeader(requestHeader));
    }

    //Validating the Channel
    if (!(doNotValidateHeaderList != null && doNotValidateHeaderList.contains(Constants.X_CHANNEL))) {
      messageList.addAll(this.validateXChannelHeader(requestHeader));
    }

    //Validating the Correlation ID
    if (!(doNotValidateHeaderList != null && doNotValidateHeaderList.contains(Constants.X_CORRELATIONID))) {
      messageList.addAll(this.validateXCorrelationIDHeader(requestHeader));
    }

    //Validating the Datetime
    if (!(doNotValidateHeaderList != null && doNotValidateHeaderList.contains(Constants.DATETIME))) {
      messageList.addAll(this.validateDateTimeHeader(requestHeader));
    }

    //Validating the Authorization
    if (!(doNotValidateHeaderList != null && doNotValidateHeaderList.contains(Constants.AUTHORIZATION))) {
      messageList.addAll(this.validateAuthorizationHeader(requestHeader));
    }

    //Validating the Authorization
    if (!(doNotValidateHeaderList != null && doNotValidateHeaderList.contains(Constants.ACCEPT_ENCODING))) {
      messageList.addAll(this.validateAcceptEncodingHeader(requestHeader));
    }

    LogUtils.logMethodExit(logger, Constants.METHOD_EXIT_SUCCESSFUL);
    return messageList;

  }

  /**
   * This method is used to validate incoming Authorization in Header.
   *
   * @param requestHeader incoming Request headers
   * @return messageList list of messages
   */
  private List<Message> validateAcceptEncodingHeader(HttpHeaders requestHeader) {
    LogUtils.logMethodEntry(logger);
    List<Message> messageList = new ArrayList<>();
    ResponseUtils responseUtils = new ResponseUtils();
    if (requestHeader.getFirst(Constants.ACCEPT_ENCODING) != null) {
      if (!Pattern.matches(ValidationRegexUtils.X_ACCEPT_ENCODING_REGEX, requestHeader.getFirst(Constants.ACCEPT_ENCODING))) {
        messageList.add(responseUtils.buildMessage(Constants.INCORRECT_HEADER_CODE, Constants.INCORRECT_HEADER_VALUE,
                Constants.INCORRECT_INVALID_ACCEPT_ENCODING));
      }
    } else {
      messageList.add(responseUtils.buildMessage(Constants.MANDATORY_HEADER_MISSING_CODE, Constants.MANDATORY_HEADER_MISSING,
              Constants.MISSING_HEADER_ACCEPT_HEADER));
    }
    LogUtils.logMethodExit(logger, Constants.METHOD_EXIT_SUCCESSFUL);
    return messageList;
  }

  /**
   * This method is used to validate incoming Authorization in Header.
   *
   * @param requestHeader incoming Request headers
   * @return messageList list of messages
   */
  private List<Message> validateAuthorizationHeader(HttpHeaders requestHeader) {
    LogUtils.logMethodEntry(logger);
    List<Message> messageList = new ArrayList<>();
    ResponseUtils responseUtils = new ResponseUtils();
    if (requestHeader.getFirst(Constants.AUTHORIZATION) != null) {
      if (requestHeader.getFirst(Constants.AUTHORIZATION).isEmpty()) {
        messageList.add(responseUtils.buildMessage(Constants.INCORRECT_HEADER_CODE, Constants.INCORRECT_HEADER_VALUE,
                Constants.INCORRECT_INVALID_AUTHORIZATION));
      }
    } else {
      messageList.add(responseUtils.buildMessage(Constants.MANDATORY_HEADER_MISSING_CODE, Constants.MANDATORY_HEADER_MISSING,
              Constants.MISSING_HEADER_AUTHORIZATION));
    }
    LogUtils.logMethodExit(logger, Constants.METHOD_EXIT_SUCCESSFUL);
    return messageList;
  }

  /**
   * * This method is used to validate incoming Correlation ID in Header.
   *
   * @param requestHeader incoming Request headers
   * @return messageList list of messages
   */
  private List<Message> validateXCorrelationIDHeader(HttpHeaders requestHeader) {
    LogUtils.logMethodEntry(logger);
    List<Message> messageList = new ArrayList<>();
    ResponseUtils responseUtils = new ResponseUtils();
    if (requestHeader.getFirst(Constants.X_CORRELATIONID) != null) {
      if (!Pattern.matches(ValidationRegexUtils.X_CORRELATION_ID_REGEX, requestHeader.getFirst(Constants.X_CORRELATIONID))) {
        messageList.add(responseUtils.buildMessage(Constants.INCORRECT_HEADER_CODE, Constants.INCORRECT_HEADER_VALUE,
                Constants.INCORRECT_INVALID_X_CORRELATION_ID));
      }
    } else {
      messageList.add(responseUtils.buildMessage(Constants.MANDATORY_HEADER_MISSING_CODE, Constants.MANDATORY_HEADER_MISSING,
              Constants.MISSING_HEADER_X_CORRELATION_ID));
    }
    LogUtils.logMethodExit(logger, Constants.METHOD_EXIT_SUCCESSFUL);
    return messageList;
  }

  /**
   * * This method is used to validate incoming Datetime in Header.
   *
   * @param requestHeader incoming Request headers
   * @return messageList list of messages
   */
  private List<Message> validateDateTimeHeader(HttpHeaders requestHeader) {
    LogUtils.logMethodEntry(logger);
    List<Message> messageList = new ArrayList<>();
    ResponseUtils responseUtils = new ResponseUtils();
    if (requestHeader.getFirst(Constants.DATETIME) != null) {
      if (!Pattern.matches(ValidationRegexUtils.DATETIME_REGEX, requestHeader.getFirst(Constants.DATETIME))) {
        messageList.add(responseUtils.buildMessage(Constants.INCORRECT_HEADER_CODE, Constants.INCORRECT_HEADER_VALUE,
                Constants.INCORRECT_INVALID_DATETIME));
      }
    } else {
      messageList.add(responseUtils.buildMessage(Constants.MANDATORY_HEADER_MISSING_CODE, Constants.MANDATORY_HEADER_MISSING,
              Constants.MISSING_HEADER_DATETIME));
    }
    LogUtils.logMethodExit(logger, Constants.METHOD_EXIT_SUCCESSFUL);
    return messageList;
  }

  /**
   * * This method is used to validate incoming X-Channel in Header.
   *
   * @param requestHeader incoming Request headers
   * @return messageList list of messages
   */
  private List<Message> validateXChannelHeader(HttpHeaders requestHeader) {
    LogUtils.logMethodEntry(logger);
    List<Message> messageList = new ArrayList<>();
    ResponseUtils responseUtils = new ResponseUtils();
    if (requestHeader.getFirst(Constants.X_CHANNEL) != null) {
      if (!Pattern.matches(ValidationRegexUtils.X_CHANNEL_REGEX, requestHeader.getFirst(Constants.X_CHANNEL))) {
        messageList.add(responseUtils.buildMessage(Constants.INCORRECT_HEADER_CODE, Constants.INCORRECT_HEADER_VALUE,
                Constants.INCORRECT_INVALID_X_CHANNEL));
      }
    } else {
      messageList.add(responseUtils.buildMessage(Constants.MANDATORY_HEADER_MISSING_CODE, Constants.MANDATORY_HEADER_MISSING,
              Constants.MISSING_HEADER_X_CHANNEL));
    }
    LogUtils.logMethodExit(logger, Constants.METHOD_EXIT_SUCCESSFUL);
    return messageList;
  }

  /**
   * * This method is used to validate incoming Authorization in Header.
   *
   * @param requestHeader incoming Request headers
   * @return messageList list of messages
   */
  private List<Message> validateXBrandHeader(HttpHeaders requestHeader) {
    LogUtils.logMethodEntry(logger);
    List<Message> messageList = new ArrayList<>();
    ResponseUtils responseUtils = new ResponseUtils();
    if (requestHeader.getFirst(Constants.X_BRAND) != null) {
      if (!Pattern.matches(ValidationRegexUtils.X_BRAND_REGEX, requestHeader.getFirst(Constants.X_BRAND))) {
        messageList.add(responseUtils.buildMessage(Constants.INCORRECT_HEADER_CODE, Constants.INCORRECT_HEADER_VALUE,
                Constants.INCORRECT_INVALID_X_BRAND));
      }
    } else {
      messageList.add(responseUtils.buildMessage(Constants.MANDATORY_HEADER_MISSING_CODE, Constants.MANDATORY_HEADER_MISSING,
              Constants.MISSING_HEADER_X_BRAND));
    }
    LogUtils.logMethodExit(logger, Constants.METHOD_EXIT_SUCCESSFUL);
    return messageList;
  }

  /**
   * * This method is used to validate incoming Content Type in Header.
   *
   * @param requestHeader incoming Request headers
   * @return messageList list of messages
   */
  private List<Message> validateContentTypeHeader(HttpHeaders requestHeader) {
    LogUtils.logMethodEntry(logger);
    List<Message> messageList = new ArrayList<>();
    ResponseUtils responseUtils = new ResponseUtils();
    if (requestHeader.getFirst(Constants.CONTENT_TYPE) != null) {
      if (!Pattern.matches(ValidationRegexUtils.CONTENT_TYPE_REGEX, requestHeader.getFirst(Constants.CONTENT_TYPE))) {
        messageList.add(responseUtils.buildMessage(Constants.INCORRECT_HEADER_CODE, Constants.INCORRECT_HEADER_VALUE,
                Constants.INCORRECT_INVALID_CONTENT_TYPE));
      }
    } else {
      messageList.add(responseUtils.buildMessage(Constants.MANDATORY_HEADER_MISSING_CODE, Constants.MANDATORY_HEADER_MISSING,
              Constants.MISSING_HEADER_CONTENT_TYPE));
    }
    LogUtils.logMethodExit(logger, Constants.METHOD_EXIT_SUCCESSFUL);
    return messageList;
  }

  /**
   * this method is used to validate the incoming subscription
   *
   * @param subscription subscription
   * @return Set of errors
   */
  @LoggingMetadata(goal = "validateSubscription")
  public Set<ConstraintViolation<Subscription>> validateSubscription(Subscription subscription) {
    LogUtils.logMethodEntry(logger);
    Set<ConstraintViolation<Subscription>> violationSet;
    Set<ConstraintViolation<Subscription>> masterViolationSet = new HashSet<>();
    Configuration<?> config = Validation.byDefaultProvider().configure();
    ValidatorFactory factory = config.buildValidatorFactory();
    Validator validator = factory.getValidator();
    factory.close();
    violationSet = validator.validate(subscription, GroupCommonSubscription.class);
    masterViolationSet.addAll(violationSet);

    //based on grouping perform validation here
    //this.validateRemoteService(masterViolationSet, validator, subscription);

    LogUtils.logMethodExit(logger, Constants.METHOD_EXIT_SUCCESSFUL);
    return masterViolationSet;
  }
}