package com.tmna.ct.telematics.subscription.service;

import com.tmna.ct.telematics.subscription.data.model.Subscription;
import com.tmna.ct.telematics.subscription.outbound.GetStatusResponse;
import com.tmna.ct.telematics.subscription.outbound.SubscriptionResponse;
import org.springframework.http.HttpHeaders;

public interface ISubscriptionService {
  /**
   * This method gets the Vehicle Details
   *
   * @param @httpHeaders incoming HttpHeader
   * @return GetStatusResponse
   */
  GetStatusResponse getStatusByVin(HttpHeaders httpHeaders);

  SubscriptionResponse createSubscription(Subscription subscription, HttpHeaders httpHeaders);

  SubscriptionResponse checkVinExist(HttpHeaders httpHeaders);
  
  SubscriptionResponse updateSubscription(String requestBody, HttpHeaders httpHeaders);

  GetStatusResponse getSubscriptionHistory(HttpHeaders httpHeaders);
}