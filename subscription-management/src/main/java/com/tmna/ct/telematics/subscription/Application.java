package com.tmna.ct.telematics.subscription;

import com.tc.logging.ILogService;
import com.tc.logging.LogFactory;
import com.tc.logging.annotation.LoggingMetadata;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

/**
 * The Spring Boot's entry class consisting the main method.
 *
 * @author Srini Y
 * @version 1.0
 */
@LoggingMetadata(feature = "Application")
@SpringBootApplication
@EnableMongoRepositories(basePackages = {"com.tmna.ct.telematics"})
@EnableAutoConfiguration
public class Application {

  private static final ILogService logger = LogFactory.getLogger(Application.class);

  @LoggingMetadata(goal = "main")
  public static void main(String[] args) {
    //System.setProperty("https.proxyHost", "tmsproxy.tms.toyota.com");
    //System.setProperty("https.proxyPort", "80");
    SpringApplication.run(Application.class, args);
    logger.info("Subscription Service is Started");
  }

}
