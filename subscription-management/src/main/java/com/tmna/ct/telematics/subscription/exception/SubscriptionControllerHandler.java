package com.tmna.ct.telematics.subscription.exception;

import com.tc.logging.ILogService;
import com.tc.logging.LogFactory;
import com.tc.logging.annotation.LoggingMetadata;
import com.tmna.ct.telematics.subscription.controller.SubscriptionController;
import com.tmna.ct.telematics.subscription.outbound.ErrorResponse;
import com.tmna.ct.telematics.subscription.outbound.Status;
import com.tmna.ct.telematics.subscription.utils.Constants;
import com.tmna.ct.telematics.subscription.utils.LogUtils;
import com.tmna.ct.telematics.subscription.utils.ResponseUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * Class to handle exception from controller
 *
 * @author Srini Y
 * @version 1.0
 */
@LoggingMetadata(feature = "SubscriptionControllerHandler")
@RestControllerAdvice(assignableTypes = {SubscriptionController.class})
public class SubscriptionControllerHandler {

  private static final ILogService logger = LogFactory.getLogger(SubscriptionControllerHandler.class);

  /**
   * This method is used to handle exception
   *
   * @param exception Exception thrown from controller
   * @return responseEntity
   */
  @LoggingMetadata(goal = "handleException")
  @ExceptionHandler(Exception.class)
  public ResponseEntity<ErrorResponse> handleException(Exception exception) {
    LogUtils.logMethodEntry(logger);
    ResponseUtils responseUtils = new ResponseUtils();
    Status status = responseUtils.buildSubscriptionErrorResponse();
    LogUtils.logMethodExit(logger, Constants.METHOD_EXIT_SUCCESSFUL);
    return new ResponseEntity<>(new ErrorResponse(status), HttpStatus.INTERNAL_SERVER_ERROR);
  }
}
