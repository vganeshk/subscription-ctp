package com.tmna.ct.telematics.subscription.service.impl;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmna.ct.telematics.subscription.data.model.Subscription;
import com.tmna.ct.telematics.subscription.outbound.Message;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringRunner;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * this Class consists of JUNIT cases for validationService class
 *
 * @author Srini Y
 * @version 1.0
 */
@RunWith(SpringRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ValidationServiceImplTest {

  Validator validator;
  @InjectMocks
  ValidationServiceImpl validationService;
  private HttpHeaders httpHeaders;
  private Subscription subscription;
  private String subscriptionDetailsValue;
  private List<String> doNotValidateHeaderList = new ArrayList<>();

  @Before
  public void setUp() throws Exception {

    MockitoAnnotations.initMocks(this);
    subscriptionDetailsValue = new String(
        Files.readAllBytes(Paths
            .get(ValidationServiceImplTest.class.getClassLoader().getResource("SubscriptionRequest.json").toURI())),
        Charset.forName("utf-8"));


    ObjectMapper objectMapper = new ObjectMapper();
    objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    subscription = objectMapper.readValue(subscriptionDetailsValue, Subscription.class);


    ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    validator = factory.getValidator();


    httpHeaders = new HttpHeaders();
    httpHeaders.add("CONTENT-TYPE", "application/json");
    httpHeaders.add("X-BRAND", "L");
    httpHeaders.add("X-CHANNEL", "TARP_SELF");
    httpHeaders.add("X-CORRELATIONID", "e115ea09-d48e-42df-9aee-48e69705e8c6");
    httpHeaders.add("Authorization", "icZBMDlw2XOtuTLIZkfeFi1vTE8QXr4lr8tA6b7t");
    httpHeaders.add("DATETIME", "1520911254125");
    httpHeaders.add("Accept-Encoding", "deflate");

  }

  /**
   * this method is used to validate all valid headers.
   */
  @Test
  public void shouldPassValidHeaders() {
    List<Message> messageList = validationService.validateRequestHeaders(httpHeaders, doNotValidateHeaderList);
    assertTrue(messageList.isEmpty());
  }

  /**
   * this method is used to validate invalid Authorization header.
   */
  @Test
  public void shouldFailInvalidAuthorization() {
    httpHeaders.remove("Authorization");
    List<Message> messageList = validationService.validateRequestHeaders(httpHeaders, doNotValidateHeaderList);
    assertFalse(messageList.isEmpty());
  }

  /**
   * this method is used to validate invalid datetime header.
   */
  @Test
  public void shouldFailInvalidDatetime() {
    httpHeaders.remove("DATETIME");
    List<Message> messageList = validationService.validateRequestHeaders(httpHeaders, doNotValidateHeaderList);
    assertFalse(messageList.isEmpty());
  }

  /**
   * this method is used to validate invalid Correlation header.
   */
  @Test
  public void shouldFailInvalidCorrelationId() {
    httpHeaders.remove("X-CORRELATIONID");
    List<Message> messageList = validationService.validateRequestHeaders(httpHeaders, doNotValidateHeaderList);
    assertFalse(messageList.isEmpty());
  }

  /**
   * this method is used to validate invalid Channel header.
   */
  @Test
  public void shouldFailInvalidChannel() {
    httpHeaders.remove("X-CHANNEL");
    List<Message> messageList = validationService.validateRequestHeaders(httpHeaders, doNotValidateHeaderList);
    assertFalse(messageList.isEmpty());
  }

  /**
   * this method is used to validate invalid brand header.
   */
  @Test
  public void shouldFailInvalidBrand() {
    httpHeaders.remove("X-BRAND");
    List<Message> messageList = validationService.validateRequestHeaders(httpHeaders, doNotValidateHeaderList);
    assertFalse(messageList.isEmpty());
  }

  /**
   * this method is used to validate invalid contentType header.
   */
  @Test
  public void shouldFailInvalidContentType() {
    httpHeaders.remove("CONTENT-TYPE");
    List<Message> messageList = validationService.validateRequestHeaders(httpHeaders, doNotValidateHeaderList);
    assertFalse(messageList.isEmpty());
  }

  /**
   * this method is used to validate missing Authorization header.
   */
  @Test
  public void shouldFailMissingAuthorization() {
    httpHeaders.set("Authorization", "");
    List<Message> messageList = validationService.validateRequestHeaders(httpHeaders, doNotValidateHeaderList);
    assertFalse(messageList.isEmpty());
  }

  /**
   * this method is used to validate missing datetime header.
   */

  @Test
  public void shouldFailMissingDatetime() {
    httpHeaders.set("DATETIME", "");
    List<Message> messageList = validationService.validateRequestHeaders(httpHeaders, doNotValidateHeaderList);
    assertFalse(messageList.isEmpty());
  }

  /**
   * this method is used to validate missing correlation ID header.
   */
  @Test
  public void shouldFailMissingCorrelationId() {
    httpHeaders.set("X-CORRELATIONID", "");
    List<Message> messageList = validationService.validateRequestHeaders(httpHeaders, doNotValidateHeaderList);
    assertFalse(messageList.isEmpty());
  }

  /**
   * this method is used to validate missing channel header.
   */
  @Test
  public void shouldFailMissingChannel() {
    httpHeaders.set("X-CHANNEL", "");
    List<Message> messageList = validationService.validateRequestHeaders(httpHeaders, doNotValidateHeaderList);
    assertFalse(messageList.isEmpty());
  }

  /**
   * this method is used to validate missing brand header.
   */
  @Test
  public void shouldFailMissingBrand() {
    httpHeaders.set("X-BRAND", "");
    List<Message> messageList = validationService.validateRequestHeaders(httpHeaders, doNotValidateHeaderList);
    assertFalse(messageList.isEmpty());
  }

  /**
   * this method is used to validate missing content type header.
   */

  @Test
  public void shouldFailMissingContentType() {
    httpHeaders.set("CONTENT-TYPE", "");
    List<Message> messageList = validationService.validateRequestHeaders(httpHeaders, doNotValidateHeaderList);
    assertFalse(messageList.isEmpty());
  }

  /**
   * this method is used to validate missing accept encoding in body.
   */
  @Ignore
  @Test
  public void shouldFailInvalidAcceptEncoding() {
    httpHeaders.remove("Accept-Encoding");
    Set<ConstraintViolation<Subscription>> constraintViolationSet = validationService.validateSubscription(subscription);
    assertFalse(constraintViolationSet.isEmpty());
  }

  /**
   * this method is used to validate missing accept encoding in body.
   */
  @Ignore
  @Test
  public void shouldFailMissingAcceptEncoding() {
    httpHeaders.set("Accept-Encoding", "");
    Set<ConstraintViolation<Subscription>> constraintViolationSet = validationService.validateSubscription(subscription);
    assertFalse(constraintViolationSet.isEmpty());
  }

  /**
   * this method is used to validate missing VIN in body.
   */
  @Ignore
  @Test
  public void shouldFailInvalidVIN() {
    subscription.setVin(null);
    Set<ConstraintViolation<Subscription>> constraintViolationSet = validationService.validateSubscription(subscription);
    assertFalse(constraintViolationSet.isEmpty());
  }

}