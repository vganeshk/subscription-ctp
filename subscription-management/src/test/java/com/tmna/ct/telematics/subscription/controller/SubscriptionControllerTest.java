package com.tmna.ct.telematics.subscription.controller;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmna.ct.telematics.subscription.data.model.Subscription;
import com.tmna.ct.telematics.subscription.outbound.GetStatusResponse;
import com.tmna.ct.telematics.subscription.outbound.SubscriptionResponse;
import com.tmna.ct.telematics.subscription.service.ISubscriptionService;
import com.tmna.ct.telematics.subscription.utils.ResponseUtils;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class SubscriptionControllerTest {

  @Mock
  private ISubscriptionService subscriptionService;

  @InjectMocks
  private SubscriptionController subscriptionController;

  private HttpHeaders httpHeaders;

  private String subscriptionDetailsValue;
  private String getSubscriptionDetailsValue;

  private SubscriptionResponse subscriptionResponse;
  private ResponseUtils responseUtils = new ResponseUtils();
  private ResponseEntity<SubscriptionResponse> subscriptionResponseResponseEntity;
  private ResponseEntity<String> subscriptionResponseStringEntity;
  private ObjectMapper objectMapper = new ObjectMapper();
  private Subscription subscription;

  @Before
  public void setUp() throws Exception {

    MockitoAnnotations.initMocks(this);
    subscriptionDetailsValue = new String(
            Files.readAllBytes(Paths
                    .get(SubscriptionControllerTest.class.getClassLoader().getResource("SubscriptionRequest.json").toURI())),
            Charset.forName("utf-8"));
    objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    subscription = objectMapper.readValue(subscriptionDetailsValue, Subscription.class);

    getSubscriptionDetailsValue = new String(
            Files.readAllBytes(Paths
                    .get(SubscriptionControllerTest.class.getClassLoader().getResource("GetSubscriptionSuccess.json").toURI())),
            Charset.forName("utf-8"));

    httpHeaders = new HttpHeaders();
    httpHeaders.add("CONTENT-TYPE","application/json");
    httpHeaders.add("X-BRAND","L");
    httpHeaders.add("X-CHANNEL","TARP_SELF");
    httpHeaders.add("X-CORRELATIONID","997a3649-523f-403b-82b8-4c2f53e0iu89");
    httpHeaders.add("Authorization","icZBMDlw2XOtuTLIZkfeFi1vTE8QXr4lr8tA6b7t");
    httpHeaders.add("DATETIME","1520911254125");

    subscriptionResponse = responseUtils.buildSubscriptionResponse(subscription);
  }

  @Test
  public void shouldPassProvCreationSuccess(){
    when(subscriptionService.createSubscription(subscription,httpHeaders)).thenReturn(subscriptionResponse);
    subscriptionResponseResponseEntity = subscriptionController.createSubscription(subscription, httpHeaders);
    assertNotNull(subscriptionResponseResponseEntity);
    assertEquals(HttpStatus.OK, subscriptionResponseResponseEntity.getStatusCode());
  }

  @Test
  public void shouldPassProvCreationFails(){
    subscriptionResponse.getStatus().setHttpCode(HttpStatus.BAD_REQUEST);
    when(subscriptionService.createSubscription(subscription,httpHeaders)).thenReturn(subscriptionResponse);
    subscriptionResponseResponseEntity = subscriptionController.createSubscription(subscription, httpHeaders);
    assertNotNull(subscriptionResponseResponseEntity);
    assertEquals(HttpStatus.BAD_REQUEST, subscriptionResponseResponseEntity.getStatusCode());
  }

  @Ignore
  @Test
  public void shouldGetSuccessWhenVINIsValid() throws IOException{
    httpHeaders.add("Vin","1NXBR32E63Z044747");
    GetStatusResponse getStatusResponse = objectMapper.readValue(getSubscriptionDetailsValue, GetStatusResponse.class);
    getStatusResponse.getStatus().setHttpCode(HttpStatus.OK);
    when(subscriptionService.getStatusByVin(httpHeaders)).thenReturn(getStatusResponse);
    ResponseEntity<GetStatusResponse> getStatusResponseResponseEntity = subscriptionController.getStatus(httpHeaders);
    assertNotNull(getStatusResponseResponseEntity);
    assertNotNull(getStatusResponseResponseEntity.getBody().getPayload());
    assertNotNull(getStatusResponseResponseEntity.getBody().getStatus());
    assertEquals(HttpStatus.OK,getStatusResponseResponseEntity.getStatusCode());
  }

  @Ignore
  @Test
  public void shouldGetFailureWhenVINIsNotValid() throws IOException{
    httpHeaders.add("Vin","");
    GetStatusResponse getStatusResponse = objectMapper.readValue(getSubscriptionDetailsValue, GetStatusResponse.class);
    getStatusResponse.getStatus().setHttpCode(HttpStatus.BAD_REQUEST);
    when(subscriptionService.getStatusByVin(httpHeaders)).thenReturn(getStatusResponse);
    ResponseEntity<GetStatusResponse> getStatusResponseResponseEntity = subscriptionController.getStatus(httpHeaders);
    assertNotNull(getStatusResponseResponseEntity);
    assertNotNull(getStatusResponseResponseEntity.getBody().getPayload());
    assertNotNull(getStatusResponseResponseEntity.getBody().getStatus());
    assertEquals(HttpStatus.BAD_REQUEST,getStatusResponseResponseEntity.getStatusCode());
  }

  @Test
  public void shouldUpdateSuccessWhenCorrelationIsValid(){
    when(subscriptionService.updateSubscription(subscriptionDetailsValue,httpHeaders)).thenReturn(subscriptionResponse);
    subscriptionResponseResponseEntity = subscriptionController.updateSubscription(subscriptionDetailsValue, httpHeaders);
    assertNotNull(subscriptionResponseResponseEntity);
    assertEquals(HttpStatus.OK,subscriptionResponseResponseEntity.getStatusCode());
  }

  @Test
  public void shouldGetSuccessWhenVinExists(){
    when(subscriptionService.checkVinExist(httpHeaders)).thenReturn(subscriptionResponse);
    subscriptionResponseStringEntity = subscriptionController.vinCheck(httpHeaders);
    assertNotNull(subscriptionResponseStringEntity);
    assertEquals(HttpStatus.OK,subscriptionResponseStringEntity.getStatusCode());
  }

  @Test
  public void shouldGetNotFoundWhenVinIsNotExist(){
    subscriptionResponse.getStatus().setHttpCode(HttpStatus.BAD_REQUEST);
    when(subscriptionService.checkVinExist(httpHeaders)).thenReturn(subscriptionResponse);
    subscriptionResponseStringEntity = subscriptionController.vinCheck(httpHeaders);
    assertNotNull(subscriptionResponseStringEntity);
    assertEquals(HttpStatus.OK,subscriptionResponseStringEntity.getStatusCode());
  }

}