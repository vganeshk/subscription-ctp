package com.tmna.ct.telematics.subscription.exception;

import com.tmna.ct.telematics.subscription.outbound.ErrorResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

/**
 * this class consists of JUNIT cases for subscriptionControllerHandler
 */
@RunWith(SpringRunner.class)
public class SubscriptionControllerHandlerTest {

  @InjectMocks
  SubscriptionControllerHandler subscriptionControllerHandler;

  @Test
  public void shouldThrowExceptionOnError(){
    ResponseEntity<ErrorResponse> responseResponseEntity = subscriptionControllerHandler.handleException(new NullPointerException());
    assertEquals("500",responseResponseEntity.getStatusCode().toString());
  }
}