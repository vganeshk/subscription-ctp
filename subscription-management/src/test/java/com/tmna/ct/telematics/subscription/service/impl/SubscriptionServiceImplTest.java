package com.tmna.ct.telematics.subscription.service.impl;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmna.ct.telematics.subscription.data.dao.IHistorySubscriptionDao;
import com.tmna.ct.telematics.subscription.data.dao.ISubscriptionDao;
import com.tmna.ct.telematics.subscription.data.model.HistorySubscription;
import com.tmna.ct.telematics.subscription.data.model.Subscription;
import com.tmna.ct.telematics.subscription.outbound.GetStatusResponse;
import com.tmna.ct.telematics.subscription.outbound.Message;
import com.tmna.ct.telematics.subscription.outbound.SubscriptionResponse;
import com.tmna.ct.telematics.subscription.service.IValidationService;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

/**
 * this class consist of JUNIT cases for subscriptionImpl
 *
 * @author Srini Y
 * @version 1.0
 */
@RunWith(SpringRunner.class)
public class SubscriptionServiceImplTest {

  @Mock
  private ISubscriptionDao subscriptionDao;
  @Mock
  private IHistorySubscriptionDao historySubscriptionDao;
  @InjectMocks
  private SubscriptionServiceImpl subscriptionService;
  @Mock
  private IValidationService validationService;
  private String subscriptionDetailsValue;
  private Subscription subscription;
  private HttpHeaders httpHeaders;
  private HistorySubscription historySubscription = new HistorySubscription();
  private List<String> doNotValidateHeaderList = null;
  private String charNameSet = "utf-8";

  @Before
  public void setUp() throws IOException, URISyntaxException {

    MockitoAnnotations.initMocks(this);
    subscriptionDetailsValue = new String(
        Files.readAllBytes(Paths
            .get(SubscriptionServiceImplTest.class.getClassLoader().getResource("SubscriptionRequest.json").toURI())),
        Charset.forName(charNameSet));

    ObjectMapper objectMapper = new ObjectMapper();
    objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    subscription = objectMapper.readValue(subscriptionDetailsValue, Subscription.class);

    httpHeaders = new HttpHeaders();
    httpHeaders.add("CONTENT-TYPE", "application/json");
    httpHeaders.add("X-BRAND", "L");
    httpHeaders.add("X-CHANNEL", "TARP_SELF");
    httpHeaders.add("X-CORRELATIONID", "997a3649-523f-403b-82b8-4c2f53e0iu89");
    httpHeaders.add("Authorization", "icZBMDlw2XOtuTLIZkfeFi1vTE8QXr4lr8tA6b7t");
    httpHeaders.add("DATETIME", "1520911254125");
    httpHeaders.add("Accept-Encoding", "deflate");

  }

  /**
   * this method is used to validate successfull provision creation
   */
  @Ignore
  @Test
  public void shouldPassProvCreationSuccess() {
    historySubscription.setSubscription(subscription);
    when(subscriptionDao.save(any(Subscription.class))).thenReturn(subscription);
    when(historySubscriptionDao.save(any(HistorySubscription.class))).thenReturn(historySubscription);
    SubscriptionResponse subscriptionResponse = subscriptionService.createSubscription(subscription, httpHeaders);
    assertNotNull(subscriptionResponse);
    assertEquals(HttpStatus.OK, subscriptionResponse.getStatus().getHttpCode());
  }

  @Ignore
  @Test
  public void shouldFailSubscriptionCreationInvalidHeader() {
    List<Message> messageList = new ArrayList<>();
    messageList.add(new Message());
    when(validationService.validateRequestHeaders(httpHeaders, doNotValidateHeaderList)).thenReturn(messageList);
    SubscriptionResponse subscriptionResponse = subscriptionService.createSubscription(subscription, httpHeaders);
    assertNotNull(subscriptionResponse);
    assertEquals(HttpStatus.BAD_REQUEST, subscriptionResponse.getStatus().getHttpCode());
  }

  @Ignore
  @Test
  public void shouldPassSubscriptionSearchSuccess() {
    when(subscriptionDao.findByVin(any(String.class))).thenReturn(subscription);
    httpHeaders.add("VIN", "1NXBR32E63Z044747");
    GetStatusResponse getStatusResponse = subscriptionService.getStatusByVin(httpHeaders);
    assertNotNull(getStatusResponse);
    assertEquals(HttpStatus.OK, getStatusResponse.getStatus().getHttpCode());
  }

  @Test
  public void shouldFailSubscriptionSearchInvalidVin() {
    when(subscriptionDao.findByVin(any(String.class))).thenReturn(null);
    httpHeaders.add("VIN", "1NXBR32E63Z044757");
    GetStatusResponse getStatusResponse = subscriptionService.getStatusByVin(httpHeaders);
    assertNotNull(getStatusResponse);
    assertEquals(HttpStatus.NOT_FOUND, getStatusResponse.getStatus().getHttpCode());
  }

  @Test
  public void shouldFailSubscriptionSearchInvalidHeader() {
    List<Message> messageList = new ArrayList<>();
    messageList.add(new Message());
    when(validationService.validateRequestHeaders(httpHeaders, doNotValidateHeaderList)).thenReturn(messageList);
    httpHeaders.add("VIN", "1NXBR32E63Z044767");
    GetStatusResponse getStatusResponse = subscriptionService.getStatusByVin(httpHeaders);
    assertNotNull(getStatusResponse);
    assertEquals(HttpStatus.BAD_REQUEST, getStatusResponse.getStatus().getHttpCode());
  }

  @Test
  public void shouldFailSubscriptionSearchEmptyHeader() {
    httpHeaders.add("VIN", "");
    GetStatusResponse getStatusResponse = subscriptionService.getStatusByVin(httpHeaders);
    assertNotNull(getStatusResponse);
    assertEquals(HttpStatus.NOT_FOUND, getStatusResponse.getStatus().getHttpCode());
  }

  @Test
  public void shouldFailCheckVINIsNotValid() {
    when(subscriptionDao.existsByVin(any(String.class))).thenReturn(false);
    SubscriptionResponse subscriptionResponse = subscriptionService.checkVinExist(httpHeaders);
    assertNotNull(subscriptionResponse);
    assertEquals(HttpStatus.NOT_FOUND, subscriptionResponse.getStatus().getHttpCode());
  }

  @Test
  public void shouldPassCheckVINIsValid() {
    when(subscriptionDao.existsByVin(any(String.class))).thenReturn(true);
    httpHeaders.add("Vin", "1NXBR32E63Z047747");
    SubscriptionResponse subscriptionResponse = subscriptionService.checkVinExist(httpHeaders);
    assertNotNull(subscriptionResponse);
    assertEquals(HttpStatus.OK, subscriptionResponse.getStatus().getHttpCode());
  }

  @Test
  public void shouldFailCheckVINInvalidHeader() {
    List<Message> messageList = new ArrayList<>();
    messageList.add(new Message());
    when(validationService.validateRequestHeaders(httpHeaders, doNotValidateHeaderList)).thenReturn(messageList);
    httpHeaders.add("Vin", "1NXBR32E63Z048747");
    SubscriptionResponse subscriptionResponse = subscriptionService.checkVinExist(httpHeaders);
    assertNotNull(subscriptionResponse);
    assertEquals(HttpStatus.BAD_REQUEST, subscriptionResponse.getStatus().getHttpCode());
  }

}